//GPU.h
//copyright (c) 2009 Stephan Schmidt

#include "Kernels.cu"

bool GPUInitialized = false;
dim3 MatVecGrid;
dim3 MatVecBlock;

REAL *d_ResReduction = NULL;

__host__ void InitGPU(REAL EleStiff[24][24])
{
	cout << endl << "Initialize GPU" << endl;
	//stuff for mat vec
	if(NumX % BLOCKX != 0)
	{
		cout << "WARNING MatVec: NumX not divisible by " << BLOCKX << " wastes resources"<<endl;
	}
	if(NumY % BLOCKY != 0)
	{
		cout << "WARNING MatVec: NumY not divisible by " << BLOCKY << " wastes resources"<<endl;
	}
	MatVecGrid.x = 1 + (NumX-1)/BLOCKX;
	MatVecGrid.y = 1 + (NumY-1)/BLOCKY;
	MatVecGrid.z = 1;
	MatVecBlock.x = BLOCKX;
	MatVecBlock.y = BLOCKY;
	cout << "MatVecGrid: " << MatVecGrid.x << " " << MatVecGrid.y << " " << MatVecGrid.z << endl;
	cout << "MatVecBlock: "<< MatVecBlock.x << " " << MatVecBlock.y << " " << MatVecBlock.z << endl;
	
	//stuff for scalar product
	//check if the number of threads for the scalar product is of the type 2^n
	REAL log2threads = log(SCALAR4BLOCKX*SCALAR4BLOCKY)/log(2.0);
	cout << "Num threads Scalar4: "<< SCALAR4BLOCKX*SCALAR4BLOCKY << " = 2^(" << log2threads <<")"<< endl;
	if(abs(log2threads - floor(log2threads)) > 1e-5)
	{
		cerr << "ERROR: NUMBER OF THREADS FOR SCALAR4 IS NOT OF TYPE 2^n" << endl;
		cerr << "SCALAR4 TREE REDUCTION WILL NOT WORK" << endl;
		exit(1);
	}
	if(NumX % SCALAR4BLOCKX != 0)
	{
		cout << "WARNING Scalar4: NumX not divisible by " << SCALAR4BLOCKX << " wastes resources"<<endl;
	}
	if(NumY % SCALAR4BLOCKY != 0)
	{
		cout << "WARNING Scalar4: NumY not divisible by " << SCALAR4BLOCKY << " wastes resources"<<endl;
	}
	Scalar4Grid.x = 1 + (NumX-1)/SCALAR4BLOCKX;
	Scalar4Grid.y = 1 + (NumY-1)/SCALAR4BLOCKY;
	Scalar4Grid.z = 1;
	Scalar4Block.x = SCALAR4BLOCKX;
	Scalar4Block.y = SCALAR4BLOCKY;	
	cout << "Scalar4Grid: " << Scalar4Grid.x << " " << Scalar4Grid.y << " " << Scalar4Grid.z << endl;
	cout << "Scalar4Block: "<< Scalar4Block.x << " " << Scalar4Block.y << " " << Scalar4Block.z << endl;
	
	//For scalar product reduction:
	//compute the padding of Scalar4Grid to the next power of 2:
	REAL p2exp = log(Scalar4Grid.x*Scalar4Grid.y)/log(2.0);
	REAL p2temp = ceil(p2exp);
	REAL p2round = pow(2, p2temp);
	cout << "Check for Scalar4 Reduction Algorithm: "<< Scalar4Grid.x*Scalar4Grid.y <<", p2exp: " << p2exp << ", p2temp: " << p2temp << ", round to 2^n: "<<p2round << endl;
	if(p2round > p2)
	{
		cerr << "ERROR: Too Many threads for single stage reduction!"<<endl;
		cerr << "Reduce NumX or NumY or increase SCALAR4BLOCKX or increase SCALAR4BLOCKY"<<endl;
		cerr << "Or program recoursive reduction!"<<endl;
		exit(1);
	}
	cudaMalloc((void**)&d_ResReduction, p2*sizeof(REAL));

	//copy Reference Stiffness
	cout << "Copy reference stiffness" << endl;
	cudaMemcpyToSymbol(GPU_EleStiff, EleStiff, 24*24*sizeof(REAL));
	cout << "GPU initialized" << endl;
	cout << endl;
	GPUInitialized = true;
}

//computes the scalar product Ax inside the GPU
__host__ inline void GPUMatVec(REAL4 *d_u, REAL4 *d_res, size_t pitch_bytes)
{
#ifdef MeasureTime
    MatVecCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
	const int pitch = pitch_bytes/sizeof(REAL4);
	MatVecKernel<<<MatVecGrid, MatVecBlock>>>(NumX, NumY, NumZ, pitch, d_u, d_res, pexp);
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    MatVecTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

//computes the residual b-Ax inside the GPU
__host__ inline void GPURes(REAL4 *d_u, REAL4 *d_res, size_t pitch_bytes)
{
#ifdef MeasureTime
    ResCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
	const int pitch = pitch_bytes/sizeof(REAL4);
	ResidualKernel<<<MatVecGrid, MatVecBlock>>>(NumX, NumY, NumZ, pitch, d_u, d_res, pexp);
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    ResTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

//computes (d_vec1)^T*(d_vec2) inside the GPU
__host__ inline void GPUScalar(REAL* d_result, REAL4 *d_vec1, REAL4 *d_vec2, const size_t pitch_bytes)
{
#ifdef MeasureTime
    ScalarProductCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
	const int pitch = pitch_bytes/sizeof(REAL4);
	//compute component-wise product
	ScalarProd4Kernel<<<Scalar4Grid, Scalar4Block>>>(d_result, d_vec1, d_vec2, pitch, NumX, NumY, NumZ);
	cudaThreadSynchronize();
	//sum up
	MyReduction<<<1, p2>>>(d_result, d_result);
	cudaThreadSynchronize();
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    ScalarProductTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

//compute the sum of a REAL vector
__host__ inline void GPUSum(REAL *d_result, REAL *d_vec, const size_t pitch_bytes)
{
	dim3 SumBlock;
	SumBlock.x = Scalar4Block.x;
	SumBlock.y = Scalar4Block.y;
	SumBlock.z = 1;
	dim3 SumGrid;
	SumGrid.x = Scalar4Grid.x;
	SumGrid.y = Scalar4Grid.y;
	SumGrid.z = 1;
	const int pitch = pitch_bytes/sizeof(REAL);
	cudaMemset(d_result, (REAL)0.0, p2*sizeof(REAL));
	cudaThreadSynchronize();
	Sum1Kernel<<<SumGrid, SumBlock>>>(d_result, d_vec, pitch, NumX, NumY, NumZ);
	cudaThreadSynchronize();
	MyReduction<<<1, p2>>>(d_result, d_result);
	cudaThreadSynchronize();
}

//compute the volume of the state
__host__ inline void GPUVolume(REAL *d_result, REAL4 *d_u, const size_t pitch_bytes)
{
	dim3 SumBlock;
	SumBlock.x = Scalar4Block.x;
	SumBlock.y = Scalar4Block.y;
	SumBlock.z = 1;
	dim3 SumGrid;
	SumGrid.x = Scalar4Grid.x;
	SumGrid.y = Scalar4Grid.y;
	SumGrid.z = 1;
	const int pitch = pitch_bytes/sizeof(REAL4);
	cudaMemset(d_result, (REAL)0.0, p2*sizeof(REAL));
	cudaThreadSynchronize();
	SumVolumeKernel<<<SumGrid, SumBlock>>>(d_result, d_u, pitch, NumX, NumY, NumZ);
	cudaThreadSynchronize();
	MyReduction<<<1, p2>>>(d_result, d_result);
	cudaThreadSynchronize();
}

__host__ inline void VecSMultAdd(REAL4 *d_v, REAL a1, REAL4 *d_w, const REAL a2, const size_t pitch_bytes, const int NX, const int NY, const int NZ)
{
#ifdef MeasureTime
    VecSMultAddCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
	const int pitch = pitch_bytes/sizeof(REAL4);
	MyVecSMultAddKernel<<<MatVecGrid, MatVecBlock>>>(d_v, a1, d_w, a2, pitch, NumX, NumY, NumZ);
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    VecSMultAddTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

__host__ void GPUCG(REAL4 *d_u, REAL4 res[], const int iter, const int OptIter, const REAL EndRes, int &FinalIter, REAL &FinalRes, REAL EleStiff[24][24])
{
#ifdef MeasureTime
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
	cout << "Start GPU_CG-Iteration" << endl;
	size_t pitch_bytes;
	REAL4 *d_res, *d_d, *d_q;
	cudaMallocPitch((void **)&d_res, &pitch_bytes, sizeof(REAL4)*NumX, NumY*NumZ);
	cudaMallocPitch((void **)&d_d, &pitch_bytes, sizeof(REAL4)*NumX, NumY*NumZ);
	cudaMallocPitch((void **)&d_q, &pitch_bytes, sizeof(REAL4)*NumX, NumY*NumZ);

	//cout << "NumX, NumY, NumZ: " << NumX << " " << NumY << " " << NumZ<<endl;
	//cout << "pitch_bytes: " << pitch_bytes << endl;
	//cout << "sizeof(REAL4): "<< sizeof(REAL4) << endl;
	cout << "GPU memory: "<<(3*(pitch_bytes/sizeof(REAL4))*NumY*NumZ*sizeof(REAL4)+p2*sizeof(REAL))/1024/1024 << " MB allocated"<<endl;

	//initialization:
	//init Residual to zero
	cudaMemset(d_ResReduction, 0.0, p2*sizeof(REAL));
	//copy state to GPU
	//cudaMemcpy2D(d_u, pitch_bytes, u, sizeof(REAL4)*NumX, sizeof(REAL4)*NumX, NumY*NumZ, cudaMemcpyHostToDevice);
	
	int iCounter = 1;
	GPURes(d_u, d_res, pitch_bytes);
	GPUScalar(d_ResReduction, d_res, d_res, pitch_bytes);
	cudaMemcpy2D(d_d, pitch_bytes, d_res, pitch_bytes, sizeof(REAL4)*NumX, NumY*NumZ, cudaMemcpyDeviceToDevice);
	
	REAL g_ResBest;
	cudaMemcpy(&g_ResBest, d_ResReduction, sizeof(REAL), cudaMemcpyDeviceToHost);
	g_ResBest = sqrt(g_ResBest);
	
	/*
	//PRECON:
	DiagScale(d, d, DiagInv);
	*/
	
	//REAL delta_new = ScalarProduct4(res, d);
	cudaThreadSynchronize();
	GPUScalar(d_ResReduction, d_res, d_d, pitch_bytes);
	cudaThreadSynchronize();
	REAL g_delta_new;
	cudaMemcpy(&g_delta_new, d_ResReduction, sizeof(REAL), cudaMemcpyDeviceToHost);
	
#ifdef WriteCGHistory
    cout << 0 << " GPU: " << g_ResBest<< endl;
	ofstream CGhistory;
	char sBuffer[512];
	sprintf(sBuffer, "./output/CGHistory.%04d.txt", OptIter);
	CGhistory.open(sBuffer);
	CGhistory << scientific;
#endif
	
	const REAL term = EndRes*EndRes;
	while(iCounter < iter && g_delta_new > term)
	{
		//q=Ad
		GPUMatVec(d_d, d_q, pitch_bytes);
		REAL g_temp;
		GPUScalar(d_ResReduction, d_d, d_q, pitch_bytes);
		cudaThreadSynchronize();
		cudaMemcpy(&g_temp, d_ResReduction, sizeof(REAL), cudaMemcpyDeviceToHost);
		REAL g_alpha = g_delta_new/g_temp;
		VecSMultAdd(d_u, 1.0, d_d, g_alpha, pitch_bytes, NumX, NumY, NumZ);
		//recompute residual by mat-vec every n iterations to
		//prevent floating point round off errors
		/*
		if(RestartCG(iCounter))
		{
			GPURes(d_u, d_res, pitch_bytes);
		}
		//else compute residual by CG-Update formula
		else
			*/
		{
			VecSMultAdd(d_res, 1.0, d_q, -1.0*g_alpha, pitch_bytes, NumX, NumY, NumZ);
		}
		/*
		//precon
		DiagScale(s, res, DiagInv);
		*/
		REAL g_delta_old = g_delta_new;
		/*
		//precon
		delta_new = ScalarProduct4(res, s);
		*/
		GPUScalar(d_ResReduction, d_res, d_res, pitch_bytes);
		cudaThreadSynchronize();
		cudaMemcpy(&g_delta_new, d_ResReduction, sizeof(REAL), cudaMemcpyDeviceToHost);
		cudaThreadSynchronize();
#ifdef WriteCGHistory
        if(iCounter %1 == 0) cout << iCounter << " GPU: "<< sqrt(g_delta_new)<<endl;
		CGhistory << iCounter << " " << sqrt(g_delta_new) << "\n";
#endif
		REAL g_beta = g_delta_new/g_delta_old;
		VecSMultAdd(d_d, g_beta, d_res, 1.0, pitch_bytes, NumX, NumY, NumZ);
		cudaThreadSynchronize();
		iCounter++;
	}
#ifdef WriteCGHistory
	CGhistory.close();
#endif
	FinalIter = iCounter;
	FinalRes = sqrt(g_delta_new);
	
	//cleanup precon:
	//delete [] DiagInv;
	//delete [] s;
	
	//upload results
	//cudaMemcpy2D(u, sizeof(REAL4)*NumX, d_u, pitch_bytes, sizeof(REAL4)*NumX, NumY*NumZ, cudaMemcpyDeviceToHost);

	//main cleanup
	//scalars
	//cudaFree(d_ResReduction); //this is a global variable now needed by other routines
	//vectors
	cudaFree(d_res);
	cudaFree(d_d);
	cudaFree(d_q);
#ifdef MeasureTime
    CGIterCount += iCounter;
    gettimeofday(&t, NULL);
    CGTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

//STUFF FOR OPTIMIZATION
__host__ inline void GPUCompGrad(REAL4 *d_u, REAL *d_grad, REAL &Obj, REAL &Vol, const size_t u_pitch_bytes, size_t &grad_pitch_bytes)
{
#ifdef MeasureTime
    CompGradCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
	dim3 GradGrid;
	dim3 GradBlock;
	GradGrid.x = 1 + (NumX-1)/BLOCKX;
	GradGrid.y = 1 + (NumY-1)/BLOCKY;
	GradGrid.z = 1;
	GradBlock.x = BLOCKX;
	GradBlock.y = BLOCKY;
	//printf("GradGrid.x = %d, GradGrid.y = %d, GradBlock.x = %d, GradBlock.y = %d\n", GradGrid.x, GradGrid.y, GradBlock.x, GradBlock.y);

	//memory for objective
	REAL *d_ObjValue;
	cudaMallocPitch((void **)&d_ObjValue, &grad_pitch_bytes, sizeof(REAL)*NumX, NumY*NumZ);

	const int grad_pitchX = grad_pitch_bytes/sizeof(REAL);
	const int u_pitchX = u_pitch_bytes/sizeof(REAL4);
	//cout << "grad_pitchX = "<<grad_pitchX<<", u_pitchX = "<<u_pitchX<<endl;
	cudaMemset(d_grad, (REAL)0.0, grad_pitchX*NumY*NumZ*sizeof(REAL));
	cudaMemset(d_ObjValue, (REAL)0.0, grad_pitchX*NumY*NumZ*sizeof(REAL));
	cudaThreadSynchronize();
	GPUEvalGrad<<<GradGrid, GradBlock>>>(NumX, NumY, NumZ, u_pitchX, grad_pitchX, d_u, d_grad, d_ObjValue, pexp);
	cudaThreadSynchronize();
	
	//sum up the per element compliance to create the total compliance
	GPUSum(d_ResReduction, d_ObjValue, grad_pitch_bytes);
	cudaThreadSynchronize();
	cudaMemcpy(&Obj, d_ResReduction, sizeof(REAL), cudaMemcpyDeviceToHost);

	//compute volume
	//memory for volume
	GPUVolume(d_ResReduction, d_u, u_pitch_bytes);
	cudaThreadSynchronize();
	cudaMemcpy(&Vol, d_ResReduction, sizeof(REAL), cudaMemcpyDeviceToHost);
	cudaFree(d_ObjValue);
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    CompGradTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

void CopyToHost(REAL4 u[], REAL Grad[], REAL4 *d_u, REAL *d_grad, size_t pitch_bytes, size_t grad_pitch_bytes)
{
#ifdef MeasureTime
    CopyToHostCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
	cudaMemcpy2D(u, sizeof(REAL4)*NumX, d_u, pitch_bytes, sizeof(REAL4)*NumX, NumY*NumZ, cudaMemcpyDeviceToHost);
	cudaMemcpy2D(Grad, sizeof(REAL)*NumX, d_grad, grad_pitch_bytes, sizeof(REAL)*NumX, NumY*NumZ, cudaMemcpyDeviceToHost);
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    CopyToHostTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

void CopyToDevice(REAL4 u[], REAL Grad[], REAL4 *d_u, REAL *d_grad, size_t pitch_bytes, size_t grad_pitch_bytes)
{
#ifdef MeasureTime
    CopyToDeviceCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
	cudaMemcpy2D(d_u, pitch_bytes, u, sizeof(REAL4)*NumX, sizeof(REAL4)*NumX, NumY*NumZ, cudaMemcpyHostToDevice);
	cudaMemcpy2D(d_grad, grad_pitch_bytes, Grad, sizeof(REAL)*NumX, sizeof(REAL)*NumX, NumY*NumZ, cudaMemcpyHostToDevice);
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    CopyToDeviceTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

void GPUCleanUp()
{
	cudaFree(d_ResReduction);
}
