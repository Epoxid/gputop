//Engine.h
//copyright (c) 2009 Stephan Schmidt

const REAL E = 1.0; //Young Modulus
const REAL nu = 0.3; //Poisson's Ratio
const REAL pexp = 3.0; //penalty exponent
const REAL VolumenFraction = 0.3;//0.2;
const REAL FilterRadius = 3.0;//2.8;//2.2;
const REAL MinDens = 0.2;//0.1; //minimum density to prevent system from becoming singular

//Returns the unique global node ID (array index) for the node in (i,j,k) coordinates
inline int GetGlobalID(int i, int j, int k)
{
  return i+j*NumX+k*NumX*NumY;
}

inline int GetLocalID(int i, int j, int k)
{
  return i+2*j+4*k;
}

#include <sys/time.h>

#include "output.h"
#include "Stiffness.h"
#include "opt.h"

REAL ScalarProduct4(REAL4 a[], REAL4 b[])
{
  REAL result = 0.0;
  #pragma omp parallel for reduction(+:result)
  for(int i=0;i<NumX*NumY*NumZ;i++)
  {
    result += a[i].x*b[i].x + a[i].y*b[i].y + a[i].z*b[i].z;
  }
  return result;
}

REAL NormRes(REAL4 Res[])
{
  return sqrt(ScalarProduct4(Res, Res));
}

//compute Residual "res = b-Au"
void ComputeResidual(REAL4 u[], REAL4 res[], register REAL EleStiff[24][24])
{
#ifdef MeasureTime
    ResCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
#pragma omp parallel for
    for(int i=0;i<NumX*NumY*NumZ;i++)
    {
        res[i].x = 0.0;
        res[i].y = 0.0;
        res[i].z = 0.0;
    }
    //access pattern: my1Darray[k*sizex*sizey + j*sizex + i]
    //Iterate over all elements
#pragma omp parallel for
    for(int k=0;k<NumZ-1;k++)
    {
        for(int j=0;j<NumY-1;j++)
        {
            for(int i=0;i<NumX-1;i++)
            {
                register REAL Dens = pow(u[GetGlobalID(i, j, k)].w, pexp);//0.0;
                //Test Functions
                for(int ek1=0;ek1<2;ek1++)
                {
                    for(int ej1=0;ej1<2;ej1++)
                    {
                        for(int ei1=0;ei1<2;ei1++)
                        {
                            const register int GID1 = GetGlobalID(i+ei1, j+ej1, k+ek1);
                            const register int LID1 = GetLocalID(ei1, ej1, ek1);
                            register REAL value1 = 0.0;
                            register REAL value2 = 0.0;
                            register REAL value3 = 0.0;
                            //Trial Functions
                            for(register int ek2=0;ek2<2;ek2++)
                            {
                                for(register int ej2=0;ej2<2;ej2++)
                                {
                                    for(register int ei2=0;ei2<2;ei2++)
                                    {
                                        const register int GID2 = GetGlobalID(i+ei2, j+ej2, k+ek2);
                                        const register int LID2 = GetLocalID(ei2, ej2, ek2);
                                        value1 += EleStiff[LID1][LID2]*u[GID2].x + EleStiff[LID1][LID2+8]*u[GID2].y + EleStiff[LID1][LID2+16]*u[GID2].z;
                                        value2 += EleStiff[LID1+8][LID2]*u[GID2].x + EleStiff[LID1+8][LID2+8]*u[GID2].y + EleStiff[LID1+8][LID2+16]*u[GID2].z;
                                        value3 += EleStiff[LID1+16][LID2]*u[GID2].x + EleStiff[LID1+16][LID2+8]*u[GID2].y + EleStiff[LID1+16][LID2+16]*u[GID2].z;
                                    }
                                }
                            }
                            res[GID1].x -= Dens*value1;
                            res[GID1].y -= Dens*value2;
                            res[GID1].z -= Dens*value3;
                            //End Trial Loop
                        }
                    }
                }
                //End Test Loop
            }
        }
    }
    //Boundary Conditions:
#pragma omp parallel for
    for(int j=0;j<NumY;j++)
    {
        for(int i=0;i<NumX;i++)
        {
            //"Front", aka k=0 gets inhomogenous neutral
            //res[GetGlobalID(i,j,0)].x += 0.0;//-1.0*u[GetGlobalID(i,j,0)].x;
            //res[GetGlobalID(i,j,0)].y += -1.0;//-1.0*u[GetGlobalID(i,j,0)].y;
            //res[GetGlobalID(i,j,0)].z += 0.0;//-1.0*u[GetGlobalID(i,j,0)].z;
            //"BACK", aka k=NumZ-1 gets Dirichlet
            res[GetGlobalID(i,j,NumZ-1)].x = -1.0*u[GetGlobalID(i,j,NumZ-1)].x;
            res[GetGlobalID(i,j,NumZ-1)].y = -1.0*u[GetGlobalID(i,j,NumZ-1)].y;
            res[GetGlobalID(i,j,NumZ-1)].z = -1.0*u[GetGlobalID(i,j,NumZ-1)].z;
        }
    }
//#pragma omp parallel for
    //for(int j=0.49*NumY;j<0.51*NumY;j++)
    //{
        for(int i=0;i<NumX;i++)
        {
            //"Front", aka k=0 gets inhomogenous neutral
            //res[GetGlobalID(i,j,0)].x += 0.0;//-1.0*u[GetGlobalID(i,j,0)].x;
            res[GetGlobalID(i,0,0)].y += -1.0;//-1.0*u[GetGlobalID(i,j,0)].y;
            //res[GetGlobalID(i,j,0)].z += 0.0;//-1.0*u[GetGlobalID(i,j,0)].z;
        }
    //}
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    ResTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

//compute MatVec "Res = A*u"
void MatVec(REAL4 u[], REAL4 res[], register REAL EleStiff[24][24])
{
#ifdef MeasureTime
    MatVecCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
  //access pattern: my1Darray[k*sizex*sizey + j*sizex + i]
  //clear residual
  #pragma omp parallel for
  for(int i=0;i<NumX*NumY*NumZ;i++)
  {
    res[i].x = 0.0;
    res[i].y = 0.0;
    res[i].z = 0.0;
  }
  //Iterate over all elements
  #pragma omp parallel for
  for(int k=0;k<NumZ-1;k++)
  {
    for(int j=0;j<NumY-1;j++)
    {
      for(int i=0;i<NumX-1;i++)
      {
        const REAL Dens = pow(u[GetGlobalID(i, j, k)].w, pexp);
/*
        for(register int ek1=0;ek1<2;ek1++)
        {
          for(register int ej1=0;ej1<2;ej1++)
          {
            for(register int ei1=0;ei1<2;ei1++)
            {
              Dens += u[GetGlobalID(i+ei1, j+ej1, k+ek1)].w;
            }
          }
        }
        Dens *= 1.0/8.0;
*/
        //Test Functions
        for(int ek1=0;ek1<2;ek1++)
        {
          for(int ej1=0;ej1<2;ej1++)
          {
            for(int ei1=0;ei1<2;ei1++)
            {
              const register int GID1 = GetGlobalID(i+ei1, j+ej1, k+ek1);
              const register int LID1 = GetLocalID(ei1, ej1, ek1);
              register REAL value1 = 0.0;
              register REAL value2 = 0.0;
              register REAL value3 = 0.0;
              //Trial Functions
              for(register int ek2=0;ek2<2;ek2++)
              {
                for(register int ej2=0;ej2<2;ej2++)
                {
                  for(register int ei2=0;ei2<2;ei2++)
                  {
                    const register int GID2 = GetGlobalID(i+ei2, j+ej2, k+ek2);
                    const register int LID2 = GetLocalID(ei2, ej2, ek2);
                    value1 += EleStiff[LID1][LID2]*u[GID2].x + EleStiff[LID1][LID2+8]*u[GID2].y + EleStiff[LID1][LID2+16]*u[GID2].z;
                    value2 += EleStiff[LID1+8][LID2]*u[GID2].x + EleStiff[LID1+8][LID2+8]*u[GID2].y + EleStiff[LID1+8][LID2+16]*u[GID2].z;
                    value3 += EleStiff[LID1+16][LID2]*u[GID2].x + EleStiff[LID1+16][LID2+8]*u[GID2].y + EleStiff[LID1+16][LID2+16]*u[GID2].z;
                  }
                }
              }
              res[GID1].x += Dens*value1;
              res[GID1].y += Dens*value2;
              res[GID1].z += Dens*value3;
              //End Trial Loop
            }
          }
        }
        //End Test Loop
      }
    }
  }
  //Boundary Conditions:
  #pragma omp parallel for
  for(int j=0;j<NumY;j++)
  {
    for(int i=0;i<NumX;i++)
    {
      //"Front", aka k=0 gets Neutral
      //res[GetGlobalID(i,j,0)].x = u[GetGlobalID(i,j,0)].x;
      //res[GetGlobalID(i,j,0)].y = u[GetGlobalID(i,j,0)].y;
      //res[GetGlobalID(i,j,0)].z = u[GetGlobalID(i,j,0)].z;
      //"BACK", aka k=NumZ-1 gets Dirichlet
      res[GetGlobalID(i,j,NumZ-1)].x = u[GetGlobalID(i,j,NumZ-1)].x;
      res[GetGlobalID(i,j,NumZ-1)].y = u[GetGlobalID(i,j,NumZ-1)].y;
      res[GetGlobalID(i,j,NumZ-1)].z = u[GetGlobalID(i,j,NumZ-1)].z;
    }
  }
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    MatVecTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

void NodalMatVec(REAL4 u[], REAL4 res[], register REAL EleStiff[24][24])
{
#ifdef MeasureTime
    NodalMatVecCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
	//loop over all k-nodes
	#pragma omp parallel for
	for(int k=0;k<NumZ;k++)
	{
		//loop over all j-nodes
		for(int j=0;j<NumY;j++)
		{
			//loop over all i-nodes
			for(int i=0;i<NumX;i++)
			{
				//get the global ID of the row the value is computed for
				const int GID1 = GetGlobalID(i,j,k);
				register REAL value1 = 0.0;
				register REAL value2 = 0.0;
				register REAL value3 = 0.0;
				//now considering the test-function with global ID i,j,k
				//get all elements this node is an edge of:
				for(int ek1=0;ek1<2;ek1++)
				{
					//get the IDs of the neighbour element we are currently considering
					const int ElementID_k = k-ek1;
					//a node with k==0 is not part of an element to its front
					for(int ej1=0;ej1<2 && ElementID_k >= 0 && ElementID_k < NumZ-1;ej1++)
					{
						//get the IDs of the neighbour element we are currently considering
						const int ElementID_j = j-ej1;
						//a node with j==0 is not part of an element to its bottom
						for(int ei1=0;ei1<2 && ElementID_j >= 0 && ElementID_j < NumY-1;ei1++)
						{
							//get the IDs of the neighbour element we are currently considering
							const int ElementID_i = i-ei1;
							//a node with i==0 is not part of an element to its left
							if(ElementID_i >= 0 && ElementID_i < NumX-1)
							{
								//Get the constant density of this element:
								const REAL Dens = pow(u[GetGlobalID(ElementID_i, ElementID_j, ElementID_k)].w, pexp);
								//Get the local 1D ID of the test function under consideration
								//Note the 3D ID is given by ei1, ej1, ek1
								const int LID1 = GetLocalID(ei1, ej1, ek1);
								//compute the FEM Test with all trial functions of this element
								//the local 3D ID of the trial functions are given by ei2, ej2, ek2
								for(int ek2=0;ek2<2;ek2++)
								{
									for(int ej2=0;ej2<2;ej2++)
									{
										for(int ei2=0;ei2<2;ei2++)
										{
											//Get the local ID of the trail function to test against
											const int LID2 = GetLocalID(ei2, ej2, ek2);
											//Get the global ID of the value to multiply with
											const int GID2 = GetGlobalID(ElementID_i+ei2, ElementID_j+ej2, ElementID_k+ek2);
											value1 += Dens*(EleStiff[LID1][LID2]*u[GID2].x + EleStiff[LID1][LID2+8]*u[GID2].y + EleStiff[LID1][LID2+16]*u[GID2].z);
											value2 += Dens*(EleStiff[LID1+8][LID2]*u[GID2].x + EleStiff[LID1+8][LID2+8]*u[GID2].y + EleStiff[LID1+8][LID2+16]*u[GID2].z);
											value3 += Dens*(EleStiff[LID1+16][LID2]*u[GID2].x + EleStiff[LID1+16][LID2+8]*u[GID2].y + EleStiff[LID1+16][LID2+16]*u[GID2].z);
										}
									}
								}//end trial for this element
							}//end if i-node is valid
						}
					}
				}//end loop all neighbour elements
				//exit(1);
				//update values:
				res[GID1].x = value1;
				res[GID1].y = value2;
				res[GID1].z = value3;
			}
		}
	}
	//Boundary Conditions:
	#pragma omp parallel for
	for(int j=0;j<NumY;j++)
	{
		for(int i=0;i<NumX;i++)
		{
			//"Front", aka k=0 gets Neutral
			//res[GetGlobalID(i,j,0)].x = u[GetGlobalID(i,j,0)].x;
			//res[GetGlobalID(i,j,0)].y = u[GetGlobalID(i,j,0)].y;
			//res[GetGlobalID(i,j,0)].z = u[GetGlobalID(i,j,0)].z;
			//"BACK", aka k=NumZ-1 gets Dirichlet
			res[GetGlobalID(i,j,NumZ-1)].x = u[GetGlobalID(i,j,NumZ-1)].x;
			res[GetGlobalID(i,j,NumZ-1)].y = u[GetGlobalID(i,j,NumZ-1)].y;
			res[GetGlobalID(i,j,NumZ-1)].z = u[GetGlobalID(i,j,NumZ-1)].z;
		}
	}
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    NodalMatVecTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

void NodalComputeResidual(REAL4 u[], REAL4 res[], register REAL EleStiff[24][24])
{
#ifdef MeasureTime
    NodalResCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
	//ComputeResidual(u, res, EleStiff);
	//return;
	//loop over all k-nodes
#pragma omp parallel for
	for(int k=0;k<NumZ;k++)
	{
		//loop over all j-nodes
		for(int j=0;j<NumY;j++)
		{
			//loop over all i-nodes
			for(int i=0;i<NumX;i++)
			{
				//get the global ID of the row the value is computed for
				const int GID1 = GetGlobalID(i,j,k);
				register REAL value1 = 0.0;
				register REAL value2 = 0.0;
				register REAL value3 = 0.0;
				//now considering the test-function with global ID i,j,k
				//get all elements this node is an edge of:
				for(int ek1=0;ek1<2;ek1++)
				{
					//get the IDs of the neighbour element we are currently considering
					const int ElementID_k = k-ek1;
					//a node with k==0 is not part of an element to its front
					for(int ej1=0;ej1<2 && ElementID_k >= 0 && ElementID_k < NumZ-1;ej1++)
					{
						//get the IDs of the neighbour element we are currently considering
						const int ElementID_j = j-ej1;
						//a node with j==0 is not part of an element to its bottom
						for(int ei1=0;ei1<2 && ElementID_j >= 0 && ElementID_j < NumY-1;ei1++)
						{
							//get the IDs of the neighbour element we are currently considering
							const int ElementID_i = i-ei1;
							//a node with i==0 is not part of an element to its left
							if(ElementID_i >= 0 && ElementID_i < NumX-1)
							{
								//Get the constant density of this element:
								const REAL Dens = pow(u[GetGlobalID(ElementID_i, ElementID_j, ElementID_k)].w, pexp);
								/*
								 for(register int ek1=0;ek1<2;ek1++)
								 {
								 for(register int ej1=0;ej1<2;ej1++)
								 {
								 for(register int ei1=0;ei1<2;ei1++)
								 {
								 Dens += u[GetGlobalID(i+ei1, j+ej1, k+ek1)].w;
								 }
								 }
								 }
								 Dens *= 1.0/8.0;
								 */						
								//Get the local 1D ID of the test function under consideration
								//Note the 3D ID is given by ei1, ej1, ek1
								const int LID1 = GetLocalID(ei1, ej1, ek1);
								//compute the FEM Test with all trial functions of this element
								//the local 3D ID of the trial functions are given by ei2, ej2, ek2
								for(int ek2=0;ek2<2;ek2++)
								{
									for(int ej2=0;ej2<2;ej2++)
									{
										for(int ei2=0;ei2<2;ei2++)
										{
											//Get the local ID of the trail function to test against
											const int LID2 = GetLocalID(ei2, ej2, ek2);
											//Get the global ID of the value to multiply with
											const int GID2 = GetGlobalID(ElementID_i+ei2, ElementID_j+ej2, ElementID_k+ek2);
											//cout << i << " " << j << " " << k << " Element: " << ElementID_i << " " << ElementID_j << " "<< ElementID_k<<": Local IDs:" << LID1 << " " << LID2 << ", Global IDs: " << GID1 << " " << GID2 << endl;
											value1 -= Dens*(EleStiff[LID1][LID2]*u[GID2].x + EleStiff[LID1][LID2+8]*u[GID2].y + EleStiff[LID1][LID2+16]*u[GID2].z);
											value2 -= Dens*(EleStiff[LID1+8][LID2]*u[GID2].x + EleStiff[LID1+8][LID2+8]*u[GID2].y + EleStiff[LID1+8][LID2+16]*u[GID2].z);
											value3 -= Dens*(EleStiff[LID1+16][LID2]*u[GID2].x + EleStiff[LID1+16][LID2+8]*u[GID2].y + EleStiff[LID1+16][LID2+16]*u[GID2].z);
										}
									}
								}//end trial for this element
							}//end if i-node is valid
						}
					}
				}//end loop all neighbour elements
				//update values:
				res[GID1].x = value1;
				res[GID1].y = value2;
				res[GID1].z = value3;
			}
		}
	}
	//Boundary Conditions:
#pragma omp parallel for
	for(int j=0;j<NumY;j++)
	{
		for(int i=0;i<NumX;i++)
		{
			//"Front", aka k=0 gets inhomogenous neutral
			//res[GetGlobalID(i,j,0)].x += 0.0;//-1.0*u[GetGlobalID(i,j,0)].x;
			//res[GetGlobalID(i,j,0)].y += -1.0;//-1.0*u[GetGlobalID(i,j,0)].y;
			//res[GetGlobalID(i,j,0)].z += 0.0;//-1.0*u[GetGlobalID(i,j,0)].z;
			//"BACK", aka k=NumZ-1 gets Dirichlet
			res[GetGlobalID(i,j,NumZ-1)].x = -1.0*u[GetGlobalID(i,j,NumZ-1)].x;
			res[GetGlobalID(i,j,NumZ-1)].y = -1.0*u[GetGlobalID(i,j,NumZ-1)].y;
			res[GetGlobalID(i,j,NumZ-1)].z = -1.0*u[GetGlobalID(i,j,NumZ-1)].z;
		}
	}
#pragma omp parallel for
	//for(int j=0.49*NumY;j<0.51*NumY;j++)
	//{
    for(int i=0;i<NumX;i++)
    {
		//"Front", aka k=0 gets inhomogenous neutral
		//res[GetGlobalID(i,j,0)].x += 0.0;//-1.0*u[GetGlobalID(i,j,0)].x;
		res[GetGlobalID(i,0,0)].y += -1.0;//-1.0*u[GetGlobalID(i,j,0)].y;
		//res[GetGlobalID(i,j,0)].z += 0.0;//-1.0*u[GetGlobalID(i,j,0)].z;
    }
	//}
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    NodalResTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

void ComputeDiagonal(REAL4 u[], REAL4 DiagInv[], register REAL EleStiff[24][24])
{
  #pragma omp parallel for
  for(int i=0;i<NumX*NumY*NumZ;i++)
  {
    DiagInv[i].x = 0.0;
    DiagInv[i].y = 0.0;
    DiagInv[i].z = 0.0;
  }
  //Iterate over all elements
  #pragma omp parallel for
  for(int k=0;k<NumZ-1;k++)
  {
    for(int j=0;j<NumY-1;j++)
    {
      for(int i=0;i<NumX-1;i++)
      {
        register REAL Dens = pow(u[GetGlobalID(i, j, k)].w, pexp);// 0.0;
/*
        for(register int ek1=0;ek1<2;ek1++)
        {
          for(register int ej1=0;ej1<2;ej1++)
          {
            for(register int ei1=0;ei1<2;ei1++)
            {
              Dens += u[GetGlobalID(i+ei1, j+ej1, k+ek1)].w;
            }
          }
        }
        Dens *= 1.0/8.0;
*/
        //Test Functions
        for(int ek1=0;ek1<2;ek1++)
        {
          for(int ej1=0;ej1<2;ej1++)
          {
            for(int ei1=0;ei1<2;ei1++)
            {
              const register int GID1 = GetGlobalID(i+ei1, j+ej1, k+ek1);
              const register int LID1 = GetLocalID(ei1, ej1, ek1);
              register REAL value1 = 0.0;
              register REAL value2 = 0.0;
              register REAL value3 = 0.0;
              //Trial Functions

              for(register int ek2=0;ek2<2;ek2++)
              {
                for(register int ej2=0;ej2<2;ej2++)
                {
                  for(register int ei2=0;ei2<2;ei2++)
                  {

                    const register int GID2 = GetGlobalID(i+ei2, j+ej2, k+ek2);
                    const register int LID2 = GetLocalID(ei2, ej2, ek2);

                    //value1 += EleStiff[LID1][LID2]*u[GID2].x + EleStiff[LID1][LID2+8]*u[GID2].y + EleStiff[LID1][LID2+16]*u[GID2].z;
                    //value1 += abs(EleStiff[LID1][LID2]) + abs(EleStiff[LID1][LID2+8]) + abs(EleStiff[LID1][LID2+16]);
                    //value1 += EleStiff[LID1][LID2] + EleStiff[LID1][LID2+8] + EleStiff[LID1][LID2+16];
                    value1 += EleStiff[LID1][LID1];
                    
                    //value2 += EleStiff[LID1+8][LID2]*u[GID2].x + EleStiff[LID1+8][LID2+8]*u[GID2].y + EleStiff[LID1+8][LID2+16]*u[GID2].z;
                    //value2 += abs(EleStiff[LID1+8][LID2]) + abs(EleStiff[LID1+8][LID2+8]) + abs(EleStiff[LID1+8][LID2+16]);
                    //value2 += EleStiff[LID1+8][LID2] + EleStiff[LID1+8][LID2+8] + EleStiff[LID1+8][LID2+16];
                    value2 += EleStiff[LID1+8][LID1+8];

                    //value3 += EleStiff[LID1+16][LID2]*u[GID2].x + EleStiff[LID1+16][LID2+8]*u[GID2].y + EleStiff[LID1+16][LID2+16]*u[GID2].z;
                    //value3 += abs(EleStiff[LID1+16][LID2]) + abs(EleStiff[LID1+16][LID2+8]) + abs(EleStiff[LID1+16][LID2+16]);
                    //value3 += EleStiff[LID1+16][LID2] + EleStiff[LID1+16][LID2+8] + EleStiff[LID1+16][LID2+16];
                    value3 += EleStiff[LID1+16][LID1+16];
                  }
                }
              }

              DiagInv[GID1].x += Dens*value1;
              DiagInv[GID1].y += Dens*value2;
              DiagInv[GID1].z += Dens*value3;
              //End Trial Loop
            }
          }
        }
        //End Test Loop
      }
    }
  }
  //Boundary Conditions:
  #pragma omp parallel for
  for(int j=0;j<NumY;j++)
  {
    for(int i=0;i<NumX;i++)
    {
      //"Front", aka k=0 gets Neutral
      //res[GetGlobalID(i,j,0)].x = u[GetGlobalID(i,j,0)].x;
      //res[GetGlobalID(i,j,0)].y = u[GetGlobalID(i,j,0)].y;
      //res[GetGlobalID(i,j,0)].z = u[GetGlobalID(i,j,0)].z;
      //"BACK", aka k=NumZ-1 gets Dirichlet
      DiagInv[GetGlobalID(i,j,NumZ-1)].x = 1.0;
      DiagInv[GetGlobalID(i,j,NumZ-1)].y = 1.0;
      DiagInv[GetGlobalID(i,j,NumZ-1)].z = 1.0;
    }
  }
  //invert lumped mass matrix
  #pragma omp parallel for
  for(int i=0;i<NumX*NumY*NumZ;i++)
  {
    DiagInv[i].x = 1.0/DiagInv[i].x;
    DiagInv[i].y = 1.0/DiagInv[i].y;
    DiagInv[i].z = 1.0/DiagInv[i].z;
  }
}

void DiagScale(REAL4 NewU[], REAL4 u[], REAL4 DiagInv[])
{
  #pragma omp parallel for
  for(int i=0;i<NumX*NumY*NumZ;i++)
  {
    NewU[i].x = u[i].x*DiagInv[i].x;
    NewU[i].y = u[i].y*DiagInv[i].y;
    NewU[i].z = u[i].z*DiagInv[i].z;
  }
}

bool inline RestartCG(const int iteration)
{
	//if(iteration % 50 == 0) return true;
	return false;
}

void CG(REAL4 u[], REAL4 res[], const int iter, const int OptIter, REAL EndRes, int &FinalIter, REAL &StartRes, REAL &FinalRes, register REAL EleStiff[24][24])
{
#ifdef MeasureTime
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
    const unsigned long size = NumX*NumY*NumZ;
    //Diagonal Preconditioner
    /*
     cout << "Computing Preconditioner" << endl;
     REAL4 *DiagInv = new REAL4[size];
     ComputeDiagonal(u, DiagInv, EleStiff);
	*/

    REAL4 *s = new REAL4[size];

    cout << "Start CG-Iteration" << endl;
    int iCounter = 1;
    //ComputeResidual(u, res, EleStiff);
    NodalComputeResidual(u, res, EleStiff);
    StartRes = NormRes(res);
    REAL4 *d = new REAL4[size];
    REAL4 *q = new REAL4[size];
    #pragma omp parallel for
    for(int i=0;i<size;i++)
    {
        d[i].x = res[i].x;
        d[i].y = res[i].y;
        d[i].z = res[i].z;
        //copy the density!! IMPORTANT!!
        d[i].w = u[i].w;
    }

    //PRECON:
    //DiagScale(d, d, DiagInv);

    REAL delta_new = ScalarProduct4(res, d);
    //REAL delta_0 = delta_new; //for old termination criteria
    const REAL term = EndRes*EndRes;
  
#ifdef WriteCGHistory
    cout << OptIter << " " << 0 << " " << StartRes << endl;
	ofstream CGhistory;
	char sBuffer[512];
	sprintf(sBuffer, "./output/CGHistory.%04d.txt", OptIter);
	CGhistory.open(sBuffer);
	CGhistory << scientific;
#endif

    while(iCounter < iter && delta_new > term)
    {
        //q=Ad
        //MatVec(d, q, EleStiff);
        NodalMatVec(d, q, EleStiff);
        //cout << "norm d = " << NormRes(d)<<endl;
        //cout << "norm q = " << NormRes(q)<<endl;
        REAL alpha = delta_new/ScalarProduct4(d, q);
        //cout << "alpha = " << alpha << endl;
        #pragma omp parallel for
        for(int i=0;i<size;i++)
        {
            u[i].x += alpha*d[i].x;
            u[i].y += alpha*d[i].y;
            u[i].z += alpha*d[i].z;
            //do NOT update density!! IMPORTRANT!!
        }
        //recompute residual by mat-vec every n iterations to
        //prevent floating point round off errors
        if(RestartCG(iCounter))
        {
            //ComputeResidual(u, res, EleStiff);
            NodalComputeResidual(u, res, EleStiff);
        }
        //else compute residual by CG-Update formula
        else
        {
            #pragma omp parallel for
            for(int i=0;i<size;i++)
            {
                res[i].x -= alpha*q[i].x;
                res[i].y -= alpha*q[i].y;
                res[i].z -= alpha*q[i].z;
            }
        }
        //precon
        //DiagScale(s, res, DiagInv);

        REAL delta_old = delta_new;
        //precon
        //delta_new = ScalarProduct4(res, s);
		delta_new = ScalarProduct4(res, res);
#ifdef WriteCGHistory
        if(iCounter % 1 == 0) cout << OptIter<< " " << iCounter << " " << sqrt(delta_new) << endl;
        CGhistory << iCounter << " " << sqrt(delta_new) << "\n";
#endif
        REAL beta = delta_new/delta_old;
        #pragma omp parallel for
        for(int i=0;i<size;i++)
        {
            /*
             d[i].x = s[i].x+beta*d[i].x;
             d[i].y = s[i].y+beta*d[i].y;
             d[i].z = s[i].z+beta*d[i].z;
			*/
            d[i].x = res[i].x+beta*d[i].x;
			d[i].y = res[i].y+beta*d[i].y;
			d[i].z = res[i].z+beta*d[i].z;
        }
        iCounter++;
    }
#ifdef WriteCGHistory
	CGhistory.close();
#endif
    FinalIter = iCounter;
    FinalRes = sqrt(delta_new);

    //cleanup precon:
    //delete [] DiagInv;
    delete [] s;
  
    //main cleanup
    delete [] d;
    delete [] q;
#ifdef MeasureTime
    CGIterCount += iCounter;
    gettimeofday(&t, NULL);
    CGTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

void Richardson(REAL4 u[], REAL4 res[], int iter, REAL EleStiff[24][24])
{
  cout << "Start Richardson Iteration"<<endl;
  for(int j=0;j<iter;j++)
  {
    //ComputeResidual(u, res, EleStiff);
    NodalComputeResidual(u, res, EleStiff);
    //cout << "Iter: "<<j<< " Norm(Res): "<< NormRes(res) << endl;
    cout <<j<< " "<< NormRes(res) << endl;
    #pragma omp parallel for
    for(int i=0;i<NumX*NumY*NumZ;i++)
    {
      const REAL damp = 0.1;
      u[i].x += damp*res[i].x;
      u[i].y += damp*res[i].y;
      u[i].z += damp*res[i].z;
    }
  }
}
