//DataTypes.h
//copyright (c) 2009 Stephan Schmidt

// Depending on compiler, define the __host_
// and __device__ intrinsics, otherwise just make the functions static to prevent
// linkage issues (duplicate symbols and such)
#ifdef __CUDACC__
#define HOST __host__
#define DEVICE __device__
#define HOSTDEVICE __host__ __device__
#else
#define HOST static
#define DEVICE static
#define HOSTDEVICE static
#endif

//DataTypes:

//data types for CUDA compilation
#ifdef __CUDACC__
#ifdef SinglePrecision
typedef float REAL;
typedef float4 REAL4;
typedef float3 REAL3;
#else
typedef double REAL;
typedef double4 REAL4;
typedef double3 REAL3;
#endif
#else
//data types for standard C/C++ compiler (GCC)
#ifdef SinglePrecision
typedef float REAL;
#else
typedef double REAL;
#endif

struct REAL4
{
	REAL x; 
	REAL y;
	REAL z;
	REAL w;
	
	friend REAL4 operator+(REAL4 &a, REAL4 &b)
	{
		REAL4 result;
		result.x = a.x+b.x;
		result.y = a.y+b.y;
		result.z = a.z+b.z;
		result.w = a.w+b.w;
		return result;
	}
};

struct REAL3
{
	REAL x; 
	REAL y;
	REAL z;
	
	friend REAL3 operator+(REAL3 &a, REAL3 &b)
	{
		REAL3 result;
		result.x = a.x+b.x;
		result.y = a.y+b.y;
		result.z = a.z+b.z;
		return result;
	}
};
#endif
