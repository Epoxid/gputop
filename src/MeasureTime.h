#ifdef MeasureTime
#include <sys/time.h>
#include <time.h>

unsigned long CGIterCount = 0;
long long CGTime = 0;
unsigned long CompGradCount = 0;
long long CompGradTime = 0;
unsigned long CopyToDeviceCount = 0;
long long CopyToDeviceTime = 0;
unsigned long CopyToHostCount = 0;
long long CopyToHostTime = 0;
unsigned long MatVecCount = 0;
long long MatVecTime = 0;
unsigned long NodalMatVecCount = 0;
long long NodalMatVecTime = 0;
unsigned long ResCount = 0;
long long ResTime = 0;
unsigned long NodalResCount = 0;
long long NodalResTime = 0;
unsigned long MeshFilterCount = 0;
long long MeshFilterTime = 0;
unsigned long UpdateCount = 0;
long long UpdateTime = 0;
unsigned long ScalarProductCount = 0;
long long ScalarProductTime = 0;
unsigned long VecSMultAddCount = 0;
long long VecSMultAddTime = 0;

void PrintTime()
{
    ofstream TimeOutput;
	TimeOutput.open("./output/time.txt");
    TimeOutput << "Result of Time Measurement (1 second = 1,000,000 microseconds):"<<endl;
    TimeOutput << "NumX = " << NumX <<", NumY = " << NumY << ", NumZ = " << NumZ << endl;
#ifdef UseGPU
    TimeOutput << "BLOCKX = " << BLOCKX << ", BLOCKY = " << BLOCKY << endl;
    TimeOutput << "SCALAR4BLOCKX = " << SCALAR4BLOCKX << ", SCALAR4BLOCKY = " << SCALAR4BLOCKY << "\n" << endl;
#endif
#ifdef SinglePrecision
	TimeOutput << "32-bit single precision\n"<<endl;
#else
	TimeOutput << "64-bit double precision\n"<<endl;
#endif
    
    TimeOutput << "Total number of CG Iterations: " << CGIterCount<<endl;
    if(CGIterCount > 0)
    {
        TimeOutput << "Time needed: " << CGTime << " microseconds" << endl;
        TimeOutput << "Time per CG Iter: " << ((double)CGTime)/((double)CGIterCount)<<" microseconds"<<endl;
        TimeOutput << "Time per CG Iter: " << ((double)CGTime)/((double)CGIterCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;
    
    TimeOutput << "Total number of MatVec: " << MatVecCount<<endl;
    if(MatVecCount > 0)
    {
        TimeOutput << "Time needed: " << MatVecTime << " microseconds" << endl;
        TimeOutput << "Time per MatVec: " << ((double) MatVecTime)/((double)MatVecCount)<<" microseconds"<<endl;
        TimeOutput << "Time per MatVec: " << ((double) MatVecTime)/((double)MatVecCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;
    
    TimeOutput << "Total number of NodalMatVec: " << NodalMatVecCount<<endl;
    if(NodalMatVecCount > 0)
    {
        TimeOutput << "Time needed: " << NodalMatVecTime << " microseconds" << endl;
        TimeOutput << "Time per NodalMatVec: " << ((double) NodalMatVecTime)/((double)NodalMatVecCount)<<" microseconds"<<endl;
        TimeOutput << "Time per NodalMatVec: " << ((double) NodalMatVecTime)/((double)NodalMatVecCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;
    
    TimeOutput << "Total number of Res: " << ResCount<<endl;
    if(ResCount > 0)
    {
        TimeOutput << "Time needed: " << ResTime << " microseconds" << endl;
        TimeOutput << "Time per Res: " << ((double) ResTime)/((double)ResCount)<<" microseconds"<<endl;
        TimeOutput << "Time per Res: " << ((double) ResTime)/((double)ResCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;
    
    TimeOutput << "Total number of NodalRes: " << NodalResCount<<endl;
    if(NodalResCount > 0)
    {
        TimeOutput << "Time needed: " << NodalResTime << " microseconds" << endl;
        TimeOutput << "Time per NodalRes: " << ((double) NodalResTime)/((double)NodalResCount)<<" microseconds"<<endl;
        TimeOutput << "Time per NodalRes: " << ((double) NodalResTime)/((double)NodalResCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;    
    
    TimeOutput << "Total number of ScalarProduct: " << ScalarProductCount<<endl;
    if(ScalarProductCount > 0)
    {
        TimeOutput << "Time needed: " << ScalarProductTime << " microseconds" << endl;
        TimeOutput << "Time per Update: " << ((double) ScalarProductTime)/((double)ScalarProductCount)<<" microseconds"<<endl;
        TimeOutput << "Time per Update: " << ((double) ScalarProductTime)/((double)ScalarProductCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;
    
    TimeOutput << "Total number of VecSMultAdd: " << VecSMultAddCount<<endl;
    if(VecSMultAddCount > 0)
    {
        TimeOutput << "Time needed: " << VecSMultAddTime << " microseconds" << endl;
        TimeOutput << "Time per Update: " << ((double) VecSMultAddTime)/((double)VecSMultAddCount)<<" microseconds"<<endl;
        TimeOutput << "Time per Update: " << ((double) VecSMultAddTime)/((double)VecSMultAddCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;
    
    TimeOutput << "Total number of CopyToDevice: " << CopyToDeviceCount<<endl;
    if(CopyToDeviceCount > 0)
    {
        TimeOutput << "Time needed: " << CopyToDeviceTime << " microseconds" << endl;
        TimeOutput << "Time per CopyToDevice: " << ((double) CopyToDeviceTime)/((double)CopyToDeviceCount)<<" microseconds"<<endl;
        TimeOutput << "Time per CopyToDevice: " << ((double) CopyToDeviceTime)/((double)CopyToDeviceCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;
    
    TimeOutput << "Total number of CopyToHost: " << CopyToHostCount<<endl;
    if(CopyToHostCount > 0)
    {
        TimeOutput << "Time needed: " << CopyToHostTime << " microseconds" << endl;
        TimeOutput << "Time per CopyToHost: " << ((double) CopyToHostTime)/((double)CopyToHostCount)<<" microseconds"<<endl;
        TimeOutput << "Time per CopyToHost: " << ((double) CopyToHostTime)/((double)CopyToHostCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;
    
    TimeOutput << "Total number of CompGrad: " << CompGradCount<<endl;
    if(CompGradCount > 0)
    {
        TimeOutput << "Time needed: " << CompGradTime << " microseconds" << endl;
        TimeOutput << "Time per CompGrad: " << ((double) CompGradTime)/((double)CompGradCount)<<" microseconds"<<endl;
        TimeOutput << "Time per CompGrad: " << ((double) CompGradTime)/((double)CompGradCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;
    
    TimeOutput << "Total number of MeshFilter: " << MeshFilterCount<<endl;
    if(MeshFilterCount > 0)
    {
        TimeOutput << "Time needed: " << MeshFilterTime << " microseconds" << endl;
        TimeOutput << "Time per MeshFilter: " << ((double) MeshFilterTime)/((double)MeshFilterCount)<<" microseconds"<<endl;
        TimeOutput << "Time per MeshFilter: " << ((double) MeshFilterTime)/((double)MeshFilterCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;
    
    TimeOutput << "Total number of Update: " << UpdateCount<<endl;
    if(MeshFilterCount > 0)
    {
        TimeOutput << "Time needed: " << UpdateTime << " microseconds" << endl;
        TimeOutput << "Time per Update: " << ((double) UpdateTime)/((double)UpdateCount)<<" microseconds"<<endl;
        TimeOutput << "Time per Update: " << ((double) UpdateTime)/((double)UpdateCount*1000000)<<" seconds\n"<<endl;
    }
    else TimeOutput << endl;

    TimeOutput.close();
}
#endif