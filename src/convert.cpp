//convert.cpp
//copyright (c) 2009 Stephan Schmidt

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

//Mesh dimensions as global variables
int NumX;
int NumY;
int NumZ;

//#define SinglePrecision

#include "DataTypes.h"
#include "Gauss.h"
#include "Engine.h"

int main(int argc, char **argv)
{
	if(argc != 2)
	{
		cerr << "ERROR READING COMMAND LINE"<<endl;
		exit(1);
	}
	REAL4 *u;
	int NumX, NumY, NumZ;
	if(ReadBinary(argv[1], &NumX, &NumY, &NumZ, &u))
	{
		cerr << "ERROR READING BINARY FILE. ABORT"<<endl;
		exit(1);
	}
	//WriteVTI4D(argv[1], NumX, NumY, NumZ, u);
	WriteVTI4DPadded(argv[1], NumX, NumY, NumZ, u);
	return 0;
}
