//main.cpp
//copyright (c) 2009 Stephan Schmidt

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include <sys/time.h>

//Mesh dimensions as global variables
int NumX;
int NumY;
int NumZ;

//#define SinglePrecision

#define checks
#define MeasureTime
#define WriteCGHistory

using namespace std;

#include "DataTypes.h"
#ifdef MeasureTime
#include "MeasureTime.h"
#endif
#include "Gauss.h"
#include "Engine.h"

int main(int argc, char **argv)
{
	cout << "CPU/GPU 3D Topology Optimizer" << endl;
	cout << "By Stephan Schmidt" << endl;
#ifdef SinglePrecision
	cout << "Compiled for 32-bit Single Precision"<<endl;
#else
	cout << "Compiled for 64-bit Double Precision"<<endl;
#endif
	GaussInit();
    
	cout << scientific;
	
	REAL RefStiff[3][8][3][8];
	MakeRefStiff(RefStiff);
	
	cout << "Computing local Element Stiffness of Reference Element" << endl;
	//compute element stiffness
	register REAL EleStiff[24][24];
	MakeEleStiffness(EleStiff, RefStiff);
	cout <<"Local Element Stiffness created" << endl;
	cout << "Enter number of points in x-y-z directions: ";
	cin >> NumX; cin >> NumY; cin >> NumZ;
	cout << "X: "<<NumX<<" Y: "<<NumY<<" Z: "<<NumZ<<endl;
	const unsigned long size = NumX*NumY*NumZ;
	cout << "Enter number of state iterations: ";
	int iter;
	cin >> iter;
	cout << "Enter target state residual: ";
	REAL EndRes;
	cin >> EndRes;
	cout << "Enter number of optimization iterations: ";
	int MaxOptIter;
	cin >> MaxOptIter;
	cout <<endl;
	cout << "Young Modulus           : "<<E<<endl;
	cout << "Poisson's Ratio         : "<<nu<<endl;
	cout << "Penalty Exponent        : "<<pexp<<endl;
	cout << "Volume Fraction         : "<<VolumenFraction<<endl;
	cout << "Filter Radius           : "<<FilterRadius<<endl;
	cout << "Minimum Density         : "<<MinDens<<endl;
	cout << "Total Number of Points  : "<<size<<endl;
	cout << "Total Number of Elements: "<<(NumX-1)*(NumY-1)*(NumZ-1)<<endl;
	cout << "Total Displacements     : "<<3*size<<endl;
	cout << "Total Density           : "<<size<<endl;
	cout << "Total Unknowns          : "<<4*size<<endl;
	cout << "Datatype Bytes          : "<<sizeof(REAL4)<<endl;
	cout << "Memory Requirement      : "<<size*sizeof(REAL4)/1024/1024<<" MB"<<endl;
	cout << "Iterations              : "<<iter<<endl;
	cout << "Desired Residual        : "<<EndRes<<endl;
	cout << "Volume Fraction         : "<<VolumenFraction<<endl;
#ifdef SinglePrecision
	cout << "Precision               : 32-bit single precision"<<endl;
#else
	cout << "Precision               : 64-bit double precision"<<endl<<endl;
#endif
	
	//allocate stuff:
	REAL4 *u = new REAL4[size];
	REAL4 *res = new REAL4[size];
    REAL *Grad = new REAL[NumX*NumY*NumZ];
#pragma omp parallel for
	for(int i=0;i<size;i++)
	{
		u[i].x = 0.0;
		u[i].y = 0.0;
		u[i].z = 0.0;
		u[i].w = 0.0;
        
        Grad[i] = 0.0;
	}
	
	//init density uniform to one:
#pragma omp parallel for
	for(int i=0;i<size;i++)
	{
		u[i].w = VolumenFraction;
	}
    
    ofstream history;
	history.open("./output/History.txt");
	history << scientific;

	int FinalIter = -1;
	REAL FinalRes = -1.0;
	REAL StartRes = -1.0;

	//Richardson(u, res, iter, EleStiff);
	CG(u, res, iter, 0, EndRes, FinalIter, StartRes, FinalRes, EleStiff);
    
    //NodalComputeResidual(u, res, EleStiff);
	//REAL RealRes = NormRes(res);
	cout << "CG-Residual: " << FinalRes << endl;
	//cout << "Real Residua: " << RealRes << endl;

	REAL Obj;
	REAL Obj_old;
	REAL Vol;
    
	EvalObjGrad(u, Grad, Obj, Vol, EleStiff);
	WriteBinary("./output/Data.0000.bin", NumX, NumY, NumZ, u);
	cout << "After Iter "<<0<<": Compliance = " << Obj << " Vol = "<<Vol<< endl;
	history << 0 << " " << Obj << " " << Vol << " " << FinalRes << " " << FinalIter <<endl;
	
	//start optimization
	int OptIter = 0;
	while(OptIter < MaxOptIter)
	{
		OptIter++;
		cout << "Applying Mesh Filter...";
		MeshFilter(u, FilterRadius, Grad);
		cout << " done" << endl;
		cout << "Updating Solution..."<<endl;
		Update(u, VolumenFraction, Grad);

		//Richardson(u, res, iter, EleStiff);
		CG(u, res, iter, OptIter, EndRes, FinalIter, StartRes, FinalRes, EleStiff);
        
		//NodalComputeResidual(u, res, EleStiff);
		//RealRes = NormRes(res);
		cout << "CG-Residual: " << FinalRes << endl;

		Obj_old = Obj;
        
		EvalObjGrad(u, Grad, Obj, Vol, EleStiff);
        
		cout << "After Iter "<<OptIter<<": Compliance = " << Obj << " Improvement = "<< (Obj_old-Obj)/Obj*100.0 << "% Vol = "<<Vol<<"\n"<< endl;
		char sBuffer[512];
		sprintf(sBuffer, "./output/Data.%04d.bin", OptIter);
		WriteBinary(sBuffer, NumX, NumY, NumZ, u);

		history << OptIter << " " << Obj << " " << Vol << " " << FinalRes << " " << FinalIter <<endl;
		history.flush();
#ifdef MeasureTime
		PrintTime();
#endif
	}
	history.close();
    
	delete [] Grad;
	//cleanup
	delete [] u;
	delete [] res;

	return 0;
}
