//main.cu
//copyright (c) 2009 Stephan Schmidt

#define UseGPU

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>

//Compute Capability 2.x (single precision)
#define SinglePrecision
#define BLOCKX 32
#define BLOCKY 16
#define SCALAR4BLOCKX 32
#define SCALAR4BLOCKY 32

//Compute Capability 2.x (double precision)
//#define BLOCKX 16
//#define BLOCKY 16
//#define SCALAR4BLOCKX 32
//#define SCALAR4BLOCKY 32

//Compute Capability 1.3 (single precision)
//#define SinglePrecision
//#define BLOCKX 16
//#define BLOCKY 16
//#define SCALAR4BLOCKX 32
//#define SCALAR4BLOCKY 16

//Compute Capability 1.1 (single precision)
//#define SinglePrecision
//#define BLOCKX 16
//#define BLOCKY 8 //12 ruins GPUCompGrad!
//#define SCALAR4BLOCKX 16//32
//#define SCALAR4BLOCKY 8

//Mesh dimensions as global variables
int NumX;
int NumY;
int NumZ;

#define checks
#define MeasureTime
//#define WriteCGHistory

using namespace std;

#include "DataTypes.h"
#ifdef MeasureTime
#include "MeasureTime.h"
#endif
#include "Gauss.h"
#include "Engine.h"
#include "Gpu.h"

int main(int argc, char **argv)
{
	cout << "CPU/GPU 3D Topology Optimizer" << endl;
	cout << "By Stephan Schmidt" << endl;
#ifdef SinglePrecision
	cout << "Compiled for 32-bit Single Precision"<<endl;
#else
	cout << "Compiled for 64-bit Double Precision"<<endl;
#endif
	GaussInit();

	cout << scientific;
	
	REAL RefStiff[3][8][3][8];
	MakeRefStiff(RefStiff);
	
	cout << "Computing local Element Stiffness of Reference Element" << endl;
	//compute element stiffness
	register REAL EleStiff[24][24];
	MakeEleStiffness(EleStiff, RefStiff);
	cout <<"Local Element Stiffness created" << endl;
	cout << "Enter number of points in x-y-z directions: ";
	cin >> NumX; cin >> NumY; cin >> NumZ;
	cout << "X: "<<NumX<<" Y: "<<NumY<<" Z: "<<NumZ<<endl;
	const int size = NumX*NumY*NumZ;
	cout << "Enter number of state iterations: ";
	int iter;
	cin >> iter;
	cout << "Enter target state residual: ";
	REAL EndRes;
	cin >> EndRes;
	cout << "Enter number of optimization iterations: ";
	int MaxOptIter;
	cin >> MaxOptIter;
	cout <<endl;
	cout << "Young Modulus           : "<<E<<endl;
	cout << "Poisson's Ratio         : "<<nu<<endl;
	cout << "Penalty Exponent        : "<<pexp<<endl;
	cout << "Volume Fraction         : "<<VolumenFraction<<endl;
	cout << "Filter Radius           : "<<FilterRadius<<endl;
	cout << "Minimum Density         : "<<MinDens<<endl;
	cout << "Total Number of Points  : "<<size<<endl;
	cout << "Total Number of Elements: "<<(NumX-1)*(NumY-1)*(NumZ-1)<<endl;
	cout << "Total Displacements     : "<<3*size<<endl;
	cout << "Total Density           : "<<size<<endl;
	cout << "Total Unknowns          : "<<4*size<<endl;
	cout << "Datatype Bytes          : "<<sizeof(REAL4)<<endl;
	cout << "Memory Requirement      : "<<size*sizeof(REAL4)/1024/1024<<" MB"<<endl;
	cout << "Iterations              : "<<iter<<endl;
	cout << "Desired Residual        : "<<EndRes<<endl;
	cout << "Volume Fraction         : "<<VolumenFraction<<endl;
#ifdef SinglePrecision
	cout << "Precision               : 32-bit single precision"<<endl;
#else
	cout << "Precision               : 64-bit double precision"<<endl<<endl;
#endif

	//Init GPU
	InitGPU(EleStiff);

	//allocate stuff:
	REAL4 *u = new REAL4[size];
	REAL4 *res = new REAL4[size];
	REAL *Grad = new REAL[size];
#pragma omp parallel for
	for(int i=0;i<size;i++)
	{
		u[i].x = 0.0;
		u[i].y = 0.0;
		u[i].z = 0.0;
		u[i].w = 0.0;
		
		Grad[i] = 0.0;
	}

	//init density uniform to one:
#pragma omp parallel for
	for(int i=0;i<size;i++)
	{
		u[i].w = VolumenFraction;
	}

	//Allocation of GPU Memory
	size_t pitch_bytes;
	size_t grad_pitch_bytes;
	REAL4 *d_u;
	REAL *d_grad;
	cudaMallocPitch((void **)&d_u, &pitch_bytes, sizeof(REAL4)*NumX, NumY*NumZ);
	cout << "GPU memory for state u: "<<((pitch_bytes/sizeof(REAL4))*NumY*NumZ*sizeof(REAL4)+p2*sizeof(REAL))/1024/1024 << " MB allocated"<<endl;
	cudaMallocPitch((void **)&d_grad, &grad_pitch_bytes, sizeof(REAL)*NumX, NumY*NumZ);
	cout << "GPU memory for gradient: "<<((grad_pitch_bytes/sizeof(REAL))*NumY*NumZ*sizeof(REAL))/1024/1024 << " MB allocated"<<endl;
	//GPU Memory Allocation Done
	
	ofstream history;
	history.open("./output/History.txt");
	history << scientific;

	int FinalIter = -1;
	REAL FinalRes = -1.0;

	CopyToDevice(u, Grad, d_u, d_grad, pitch_bytes, grad_pitch_bytes);
	//Richardson(u, res, iter, EleStiff);
	GPUCG(d_u, res, iter, 0, EndRes, FinalIter, FinalRes, EleStiff);

	//NodalComputeResidual(u, res, EleStiff);
	//REAL RealRes = NormRes(res);
	cout << "CG-Residual: " << FinalRes << endl;
	//cout << "Real Residua: " << RealRes << endl;

	REAL Obj;
	REAL Obj_old;
	REAL Vol;

	GPUCompGrad(d_u, d_grad, Obj, Vol, pitch_bytes, grad_pitch_bytes);
	CopyToHost(u, Grad, d_u, d_grad, pitch_bytes, grad_pitch_bytes);

	WriteBinary("./output/Data.0000.bin", NumX, NumY, NumZ, u);
	cout << "After Iter "<<0<<": Compliance = " << Obj << " Vol = "<<Vol<< endl;
	history << 0 << " " << Obj << " " << Vol << " " << FinalRes << " " << FinalIter <<endl;

	//start optimization
	int OptIter = 0;
	while(OptIter < MaxOptIter)
	{
		OptIter++;
		cout << "Applying Mesh Filter...";
		MeshFilter(u, FilterRadius, Grad);
		cout << " done" << endl;
		cout << "Updating Solution..."<<endl;
		Update(u, VolumenFraction, Grad);

		CopyToDevice(u, Grad, d_u, d_grad, pitch_bytes, grad_pitch_bytes);
		//Richardson(u, res, iter, EleStiff);
		GPUCG(d_u, res, iter, OptIter, EndRes, FinalIter, FinalRes, EleStiff);

		//NodalComputeResidual(u, res, EleStiff);
		//RealRes = NormRes(res);
		cout << "CG-Residual: " << FinalRes << endl;

		Obj_old = Obj;

		GPUCompGrad(d_u, d_grad, Obj, Vol, pitch_bytes, grad_pitch_bytes);
		CopyToHost(u, Grad, d_u, d_grad, pitch_bytes, grad_pitch_bytes);

		cout << "After Iter "<<OptIter<<": Compliance = " << Obj << " Improvement = "<< (Obj_old-Obj)/Obj*100.0 << "% Vol = "<<Vol<<"\n"<< endl;
		char sBuffer[512];
		sprintf(sBuffer, "./output/Data.%04d.bin", OptIter);
		WriteBinary(sBuffer, NumX, NumY, NumZ, u);

		history << OptIter << " " << Obj << " " << Vol << " " << FinalRes << " " << FinalIter <<endl;
		history.flush();
#ifdef MeasureTime
        PrintTime();
#endif
	}
	history.close();
  
	//CPU cleanup
	delete [] Grad;
	delete [] u;
	delete [] res;

	//GPU cleanup
	cudaFree(d_u);
	GPUCleanUp();

	return 0;
}
