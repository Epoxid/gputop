//Opt.h
//copyright (c) 2009 Stephan Schmidt

//compute objective and derivative
void EvalObjGrad(REAL4 u[], REAL Grad[], REAL &Obj, REAL &Vol, REAL EleStiff[24][24])
{
#ifdef MeasureTime
    CompGradCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
  REAL MyObj = 0.0;
  #pragma omp parallel for reduction(+:MyObj)
  for(int k=0;k<NumZ-1;k++)
  {
    for(int j=0;j<NumY-1;j++)
    {
      for(int i=0;i<NumX-1;i++)
      {
        REAL TempVec[24];
        REAL Ue[24];
        for(int itemp=0;itemp<8;itemp++)
        {
          TempVec[itemp] = 0.0;
          TempVec[itemp+8] = 0.0;
          TempVec[itemp+16] = 0.0;
        }
        const REAL dDens = pexp*pow(u[GetGlobalID(i, j, k)].w, pexp-(REAL)1.0);
        const REAL Dens = pow(u[GetGlobalID(i, j, k)].w, pexp);
/*
        for(register int ek1=0;ek1<2;ek1++)
        {
          for(register int ej1=0;ej1<2;ej1++)
          {
            for(register int ei1=0;ei1<2;ei1++)
            {
              Dens += u[GetGlobalID(i+ei1, j+ej1, k+ek1)].w;
            }
          }
        }
        Dens *= 1.0/8.0;
*/
        //Test Functions
        for(int ek1=0;ek1<2;ek1++)
        {
          for(int ej1=0;ej1<2;ej1++)
          {
            for(int ei1=0;ei1<2;ei1++)
            {
              const register int GID1 = GetGlobalID(i+ei1, j+ej1, k+ek1);
              const register int LID1 = GetLocalID(ei1, ej1, ek1);
              Ue[LID1] = u[GID1].x;
              Ue[LID1+8] = u[GID1].y;
              Ue[LID1+16] = u[GID1].z;
            }
          }
        }
        for(int counter1=0;counter1<24;counter1++)
        {
          for(int counter2=0;counter2<24;counter2++)
          {
            TempVec[counter1] += EleStiff[counter1][counter2]*Ue[counter2];
          } 
        }
        //End Test Loop
        register REAL Value = 0.0;
        for(int counter1=0;counter1<24;counter1++)
        {
          Value += Ue[counter1]*TempVec[counter1];
        }
        Grad[GetGlobalID(i, j, k)] = -1.0*dDens*Value;
        MyObj += Dens*Value;
      }
    }
  }
  Obj = MyObj;
  //compute volume
  REAL MyVol = 0.0;
  #pragma omp parallel for reduction(+:MyVol)
  for(int k=0;k<NumZ-1;k++)
  {
    for(int j=0;j<NumY-1;j++)
    {
      for(int i=0;i<NumX-1;i++)
      {
        MyVol += u[GetGlobalID(i,j,k)].w;
      }
    }
  }
  Vol = MyVol;
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    CompGradTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

void Update(REAL4 u[], REAL VolFrac, REAL Grad[])
{
#ifdef MeasureTime
    UpdateCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
  REAL *NewDens = new REAL[NumX*NumY*NumZ];
  REAL l1 = 0.0;
  REAL l2 = 1e5;
  int counter = 0;
  const REAL move = 0.2;
  while(l2-l1 > 1e-4 && counter < 1e3)
  {
    counter++;
    //cout << "Bisection: " <<counter<< " " << l2-l1<< endl;
    const REAL lmid = 0.5*(l2+l1);
    #pragma omp parallel for
    for(int k=0;k<NumZ-1;k++)
    {
      for(int j=0;j<NumY-1;j++)
      {
        for(int i=0;i<NumX-1;i++)
        {
          const register int ID = GetGlobalID(i,j,k);
          //fix for single precision round-off errors:
          //the gradient must be less than zero everywhere, so
          //everything bigger than 0.0 will be truncated
#ifdef SinglePrecision
          const register REAL MyGrad = max((REAL)-1.0*Grad[ID], (REAL) 0.0);
#else
          const register REAL MyGrad = max((REAL)-1.0*Grad[ID], (REAL) 0.0);
#endif
          NewDens[ID] = max(MinDens, max(u[ID].w-move, min((REAL)1.0, min(u[ID].w+move, u[ID].w*sqrt(MyGrad/lmid)))));
        }
      }
    }
    register REAL sum = 0.0;
    #pragma omp parallel for reduction(+:sum)
    for(int k=0;k<NumZ-1;k++)
    {
      for(int j=0;j<NumY-1;j++)
      {
        for(int i=0;i<NumX-1;i++)
        {
          sum += NewDens[GetGlobalID(i,j,k)];
        }
      }
    }
    if(sum - VolFrac*(NumX-1)*(NumY-1)*(NumZ-1) > 0)
      l1 = lmid;
    else
      l2 = lmid;
  }
  cout << "Update terminate after "<< counter << " bisections. Offset "<<l2-l1<<endl;
  //update
  #pragma omp parallel for
  for(int k=0;k<NumZ-1;k++)
  {
    for(int j=0;j<NumY-1;j++)
    {
      for(int i=0;i<NumX-1;i++)
      {
        const register int ID = GetGlobalID(i,j,k);
        u[ID].w = NewDens[ID];
      }
    }
  }
  delete [] NewDens;
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    UpdateTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

void MeshFilter(REAL4 u[], REAL rmin, REAL Grad[])
{
#ifdef MeasureTime
    MeshFilterCount++;
    struct timeval t, start_time;
    gettimeofday(&start_time, NULL);
#endif
  REAL *NewGrad = new REAL[NumX*NumY*NumZ];
  #pragma omp parallel for
  for(int i=0;i<NumX*NumY*NumZ;i++)
    NewGrad[i] = 0.0;
  #pragma omp parallel for
  for(int k=0;k<NumZ-1;k++)
  {
    for(int j=0;j<NumY-1;j++)
    {
      for(int i=0;i<NumX-1;i++)
      {
        REAL sum = 0.0;
        for(int ek = max(k-(int)rmin, 0);ek<=min(k+(int)rmin, (int)(NumZ-1));ek++)
        {
          for(int ej = max(j-(int)rmin, 0);ej<=min(j+(int)rmin, (int)(NumY-1));ej++)
          {
            for(int ei = max(i-(int)rmin, 0);ei<=min(i+(int)rmin, (int)(NumX-1));ei++)
            {
              const REAL fac = rmin-sqrt((k-ek)*(k-ek)+(j-ej)*(j-ej)+(i-ei)*(i-ei));
              sum += max((REAL)0.0, fac);
              NewGrad[GetGlobalID(i,j,k)] += max((REAL)0.0, fac)*u[GetGlobalID(ei,ej,ek)].w*Grad[GetGlobalID(ei,ej,ek)];
            }
          }
        }
        NewGrad[GetGlobalID(i,j,k)] = NewGrad[GetGlobalID(i,j,k)]/u[GetGlobalID(i,j,k)].w*sum;
      }
    }
  }
  #pragma omp parallel for
  for(int k=0;k<NumZ-1;k++)
  {
	  for(int j=0;j<NumY-1;j++)
	  {
		  for(int i=0;i<NumX-1;i++)
		  {
			  Grad[GetGlobalID(i,j,k)] = NewGrad[GetGlobalID(i,j,k)];
		  }
	  }
  }
  delete [] NewGrad;
#ifdef MeasureTime
    gettimeofday(&t, NULL);
    MeshFilterTime += (t.tv_sec - start_time.tv_sec)*1000000 + (t.tv_usec - start_time.tv_usec);
#endif
}

void AddGrad(REAL4 u[], REAL Grad[], const REAL damp)
{
	//const int size = NumX*NumY*NumZ;
	#pragma omp parallel for
	for(int k=0;k<NumZ-1;k++)
	{
		for(int j=0;j<NumY-1;j++)
		{
			for(int i=0;i<NumX-1;i++)
			{
				u[GetGlobalID(i,j,k)].w += damp*Grad[GetGlobalID(i,j,k)];
			}
		}
	}
}
