//Linear Elasticity Mat Vec GPU Kernel
//copyright (c) 2009 Stephan Schmidt

__constant__ REAL GPU_EleStiff[24][24];

#define INDEX(i,j,j_off)  (i +__mul24(j,j_off))
#define INDEX3D(i,j,k, j_off, i_off) (i + __mul24((j), (j_off)) + __mul24((k), (__mul24((i_off), (j_off)))))
#define IOFF 1
#define JOFF (BLOCKX+2)
#define KOFF ((BLOCKX+2)*(BLOCKY+2))

__global__ void MatVecKernel(const int NX, const int NY, const int NZ, const int pitchX, REAL4 *d_u, REAL4 *res, REAL gpupexp)
{
	//Strategy: One thread per node, NOT element
	//Every thread is a "compute" thread and is responsible for
	//testing its test-function with all trial function it shares a non-zero
	//support with.
	//MUST HAVE MORE INTEROUR NODES THAN OUTSIDE NODES!!
	
	//The Matrix row this thread is responsible for:
	int indg;
	//The corresponding "row" in the shared memory:
	int ind;
	
	//some threads need to load halo-values additionally to being a compute thread
	//hence they require a second set of IDs for the halos they must load
	//in addition to being a "compute" thread
	int indg_h;
	//The corresponding "row" in the share memory:
	int ind_h;
	
	__shared__ REAL4 s_u[3*KOFF];
	
	//local 1 ID index of the component being computed by this thread in this block
	int k = threadIdx.x + threadIdx.y*BLOCKX;
	//some temp variables
	int i,j;
	
	//Does this thread have to additionally load halo values?
	int halo = k < 2*(BLOCKX+BLOCKY+2);
	//if this thread must load halo values set up indexes
	//non-divergent half-warp "if", if 2*(BLOCKX+BLOCKY+2) divisible by 16, e.g. BLOCKX = 32, BLOCKY = 6
	//non-divergent warp "if", if divisible by 32, e.g. BLOCKX = 32, BLOCKY = 12
	if(halo)
	{
		//uncoalescenced y-halo: Nobrainer
		//non-divergent "if" as long as BLOCKX is a multiple of the half-warp size!!
		if(threadIdx.y<2)
		{
			//clear:
			i = threadIdx.x;
			//results in 0 for threadId.y == 0
			//results in BLOCKY+1 for threadId.y == 1
			j = threadIdx.y*(BLOCKY+1) - 1;
		}
		//x-halo uncoalescenced
		//special case: Load alternating value left or right
		else
		{
			//alternates between -1 and BLOCKX+1
			//depending if k is even or odd
			i = (k%2)*(BLOCKX+1)-1;
			//produces sequence -1, -1, 0, 0, 1, 1, 2, 2, ... , BLOCKY+1, BLOCKY+1
			j = k/2 - BLOCKX - 1;
		}
		//set the halo-index this thread needs to store to
		//in the local array, shift by +1 to prevent negative value
		ind_h = INDEX(i+1,j+1,JOFF) + KOFF;
		//set the halo-index this thread needs to load from the
		//device array
		i = INDEX(i, blockIdx.x, BLOCKX);
		j = INDEX(j, blockIdx.y, BLOCKY);
		indg_h = INDEX(i, j, pitchX);
		
		//turn off halo-loading if the halo-point to load is outside
		//the computational domain
		halo = (i>=0) && (i<NX) && (j>=0) && (j<NY);
	}
	
	//now set-up the indices a thread always has to load:
	i = threadIdx.x;
	j = threadIdx.y;
	//index in the shared array to store to
	ind = INDEX(i+1,j+1,JOFF) + KOFF;
	i = INDEX(i, blockIdx.x, BLOCKX);
	j = INDEX(j, blockIdx.y, BLOCKY);
	//index to load from in the device array
	indg = INDEX(i,j,pitchX);
	
	//shut-down if the GPU Grid is larger than the computational domain
	const int active = (i<NX) && (j<NY);
	
	//regular load operation:
	if(active) s_u[ind+KOFF] = d_u[indg];
	//halo load operation
	if(halo) s_u[ind_h+KOFF] = d_u[indg_h];
	
	//loop in z-direction
	int indg_cur;
	const int NXM1 = NX-1;
	const int NYM1 = NY-1;
	const int NZM1 = NZ-1;
	//REAL Dens;
	REAL4 MyRes;
	for(k=0;k<NZ;k++)
	{
		if(active)
		{
			//store current global index
			indg_cur = indg;
			//move down one k-plane in the global IDs
			indg = INDEX(indg, NY, pitchX);
			//copy two planes backward in the shared array
			s_u[ind-KOFF] = s_u[ind];
			s_u[ind] = s_u[ind+KOFF];
			if(k<NZM1) s_u[ind+KOFF] = d_u[indg];
		}
		if(halo)
		{
			indg_h = INDEX(indg_h, NY, pitchX);
			s_u[ind_h-KOFF] = s_u[ind_h];
			s_u[ind_h] = s_u[ind_h+KOFF];
			if(k<NZM1) s_u[ind_h+KOFF] = d_u[indg_h];
		}
		
		//shared array finally contains all data!
		__syncthreads();
		
		//compute the actual matrix-vector product
		if(active)
		{
			const REAL4 Tu = s_u[ind];
			MyRes.x = 0.0;
			MyRes.y = 0.0;
			MyRes.z = 0.0;
			MyRes.w = Tu.w;
			//NO NATURAL/NEUMANN BOUNDARY CONDITIONS IN MATVEC!
			//Dirichlet b.c.'s
			//wheel example
			//if((i<10 && j==0 && k<10) || (i>NXM1-10 && j==0 && k<10) || (i<10 && j==0 && k>NZM1-10) || (i>NXM1-10 && j==0 && k>NZM1-10))
			//cantilever example whole back wall solid
			if(k==NZM1)
			//cantilever example only corners solid
			//if(k==NZM1 && ((i<15 && j<15) || (i>NXM1-15 && j<15) || (i<15 && j >NYM1-15) || (i>NXM1-15 && j>NYM1-15)))
			{
				MyRes.x = -1.0*Tu.x;
				MyRes.y = -1.0*Tu.y;
				MyRes.z = -1.0*Tu.z;
			}
			else
			{
				for(int ek1=0;ek1<2;ek1++)
				{
					const int EIDK = k-ek1;
					if(EIDK >= 0 && EIDK < NZ-1)
					{
						for(int ej1=0;ej1<2;ej1++)
						{
							const int EIDJ = j-ej1;
							if(EIDJ >= 0 && EIDJ < NY-1)
							{
								for(int ei1=0;ei1<2;ei1++)
								{
									const int EIDI = i-ei1;
									if(EIDI >= 0 && EIDI < NX-1)
									{
										const REAL Dens = pow(s_u[ind-ei1*IOFF-ej1*JOFF-ek1*KOFF].w, gpupexp);
										const int LID1 = ei1+ej1*2+ek1*4;
										for(int ek2=0;ek2<2;ek2++)
										{
											for(int ej2=0;ej2<2;ej2++)
											{
												for(int ei2=0;ei2<2;ei2++)
												{
													const int LID2 = ei2+ej2*2+ek2*4;
													const int idiff = ei2-ei1;
													const int jdiff = ej2-ej1;
													const int kdiff = ek2-ek1;
													const REAL4 MyU = s_u[ind+idiff*IOFF+jdiff*JOFF+kdiff*KOFF];
													//const REAL4 MyU = d_u[(EIDI+ei2)+(EIDJ+ej2)*pitchX+(EIDK+ek2)*pitchX*NY];
													MyRes.x += Dens*(GPU_EleStiff[LID1][LID2]*MyU.x + GPU_EleStiff[LID1][LID2+8]*MyU.y + GPU_EleStiff[LID1][LID2+16]*MyU.z);
													MyRes.y += Dens*(GPU_EleStiff[LID1+8][LID2]*MyU.x + GPU_EleStiff[LID1+8][LID2+8]*MyU.y + GPU_EleStiff[LID1+8][LID2+16]*MyU.z);
													MyRes.z += Dens*(GPU_EleStiff[LID1+16][LID2]*MyU.x + GPU_EleStiff[LID1+16][LID2+8]*MyU.y + GPU_EleStiff[LID1+16][LID2+16]*MyU.z);
												}
											}
										}//end e2 loops
									}//if i okay
								}//i-loop
							}//if j okay
						}//j-loop
					}//if k okay
				}//k-loop
			}//end if not dirichlet
			//store results
			res[indg_cur] = MyRes;
		}//end if active
		__syncthreads();
	}
}

__global__ void ResidualKernel(const int NX, const int NY, const int NZ, const int pitchX, REAL4 *d_u, REAL4 *res, REAL gpupexp)
{
	//Strategy: One thread per node, NOT element
	//Every thread is a "compute" thread and is responsible for
	//testing its test-function with all trial function it shares a non-zero
	//support with.
	//MUST HAVE MORE INTEROUR NODES THAN OUTSIDE NODES!!
	
	//The Matrix row this thread is responsible for:
	int indg;
	//The corresponding "row" in the shared memory:
	int ind;
	
	//some threads need to load halo-values additionally to being a compute thread
	//hence they require a second set of IDs for the halos they must load
	//in addition to being a "compute" thread
	int indg_h;
	//The corresponding "row" in the share memory:
	int ind_h;
	
	__shared__ REAL4 s_u[3*KOFF];
	
	//local 1 ID index of the component being computed by this thread in this block
	int k = threadIdx.x + threadIdx.y*BLOCKX;
	//some temp variables
	int i,j;
	
	//Does this thread have to additionally load halo values?
	int halo = k < 2*(BLOCKX+BLOCKY+2);
	//if this thread must load halo values set up indexes
	//non-divergent half-warp "if", if 2*(BLOCKX+BLOCKY+2) divisible by 16, e.g. BLOCKX = 32, BLOCKY = 6
	//non-divergent warp "if", if divisible by 32, e.g. BLOCKX = 32, BLOCKY = 12
	if(halo)
	{
		//uncoalescenced y-halo: Nobrainer
		//non-divergent "if" as long as BLOCKX is a multiple of the half-warp size!!
		if(threadIdx.y<2)
		{
			//clear:
			i = threadIdx.x;
			//results in 0 for threadId.y == 0
			//results in BLOCKY+1 for threadId.y == 1
			j = threadIdx.y*(BLOCKY+1) - 1;
		}
		//x-halo uncoalescenced
		//special case: Load alternating value left or right
		else
		{
			//alternates between -1 and BLOCKX+1
			//depending if k is even or odd
			i = (k%2)*(BLOCKX+1)-1;
			//produces sequence -1, -1, 0, 0, 1, 1, 2, 2, ... , BLOCKY+1, BLOCKY+1
			j = k/2 - BLOCKX - 1;
		}
		//set the halo-index this thread needs to store to
		//in the local array, shift by +1 to prevent negative value
		ind_h = INDEX(i+1,j+1,JOFF) + KOFF;
		//set the halo-index this thread needs to load from the
		//device array
		i = INDEX(i, blockIdx.x, BLOCKX);
		j = INDEX(j, blockIdx.y, BLOCKY);
		indg_h = INDEX(i, j, pitchX);
		
		//turn off halo-loading if the halo-point to load is outside
		//the computational domain
		halo = (i>=0) && (i<NX) && (j>=0) && (j<NY);
	}
	
	//now set-up the indices a thread always has to load:
	i = threadIdx.x;
	j = threadIdx.y;
	//index in the shared array to store to
	ind = INDEX(i+1,j+1,JOFF) + KOFF;
	i = INDEX(i, blockIdx.x, BLOCKX);
	j = INDEX(j, blockIdx.y, BLOCKY);
	//index to load from in the device array
	indg = INDEX(i,j,pitchX);
	
	//shut-down if the GPU Grid is larger than the computational
	//domain
	const int active = (i<NX) && (j<NY);
	
	//regular load operation:
	if(active) s_u[ind+KOFF] = d_u[indg];
	//halo load operation
	if(halo) s_u[ind_h+KOFF] = d_u[indg_h];
	
	//loop in z-direction
	int indg_cur;
	const int NXM1 = NX-1;
	const int NYM1 = NY-1;
	const int NZM1 = NZ-1;
	//REAL Dens;
	REAL4 MyRes;
	for(k=0;k<NZ;k++)
	{
		if(active)
		{
			//store current global index
			indg_cur = indg;
			//move down one k-plane in the global IDs
			indg = INDEX(indg, NY, pitchX);
			//copy two planes backward in the shared array
			s_u[ind-KOFF] = s_u[ind];
			s_u[ind] = s_u[ind+KOFF];
			if(k<NZM1) s_u[ind+KOFF] = d_u[indg];
		}
		if(halo)
		{
			indg_h = INDEX(indg_h, NY, pitchX);
			s_u[ind_h-KOFF] = s_u[ind_h];
			s_u[ind_h] = s_u[ind_h+KOFF];
			if(k<NZM1) s_u[ind_h+KOFF] = d_u[indg_h];
		}
		
		//shared array finally contains all data!
		__syncthreads();
		
		//compute the actual matrix-vector product
		if(active)
		{
			const REAL4 Tu = s_u[ind];
			MyRes.x = 0.0;
			MyRes.y = 0.0;
			MyRes.z = 0.0;
			MyRes.w = Tu.w;
			//Neumann goes in addition to the MatVec
			//extruded cantilever
			if(k == 0 && j == 0)
			//low cantilever not extruded
			//if(abs(i-0.5*NX)<2 && j==0 && k==0)
			//standard cantilever not extruded
			//if(abs(i-0.5*NX)<4 && abs(j-0.5*NY)<4 && k==0)
			//wheel example
			//if(abs(i-0.5*NX)<2 && j==0 && abs(k-0.5*NZ)<2)
			//stool example
			//if(abs(i-0.5*NX)<2 && j==NYM1 && abs(k-0.5*NZ)<2)
			{
				MyRes.y = -1.0;
			}
			//Dirichlet b.c.'s
			//wheel and stool example
			//if((i<10 && j==0 && k<10) || (i>NXM1-10 && j==0 && k<10) || (i<10 && j==0 && k>NZM1-10) || (i>NXM1-10 && j==0 && k>NZM1-10))
			//cantilever example whole back wall solid
			if(k==NZM1)
			//cantilever example only corners solid
			//if(k==NZM1 && ((i<15 && j<15) || (i>NXM1-15 && j<15) || (i<15 && j >NYM1-15) || (i>NXM1-15 && j>NYM1-15)))
			{
				MyRes.x = -1.0*Tu.x;
				MyRes.y = -1.0*Tu.y;
				MyRes.z = -1.0*Tu.z;
			}
			else
			{
				for(int ek1=0;ek1<2;ek1++)
				{
					const int EIDK = k-ek1;
					if(EIDK >= 0 && EIDK < NZ-1)
					{
						for(int ej1=0;ej1<2;ej1++)
						{
							const int EIDJ = j-ej1;
							if(EIDJ >= 0 && EIDJ < NY-1)
							{
								for(int ei1=0;ei1<2;ei1++)
								{
									const int EIDI = i-ei1;
									if(EIDI >= 0 && EIDI < NX-1)
									{
										const REAL Dens = pow(s_u[ind-ei1*IOFF-ej1*JOFF-ek1*KOFF].w, gpupexp);
										const int LID1 = ei1+ej1*2+ek1*4;
										for(int ek2=0;ek2<2;ek2++)
										{
											for(int ej2=0;ej2<2;ej2++)
											{
												for(int ei2=0;ei2<2;ei2++)
												{
													const int LID2 = ei2+ej2*2+ek2*4;
													const int idiff = ei2-ei1;
													const int jdiff = ej2-ej1;
													const int kdiff = ek2-ek1;
													const REAL4 MyU = s_u[ind+idiff*IOFF+jdiff*JOFF+kdiff*KOFF];
													//const REAL4 MyU = d_u[(EIDI+ei2)+(EIDJ+ej2)*pitchX+(EIDK+ek2)*pitchX*NY];
													MyRes.x -= Dens*(GPU_EleStiff[LID1][LID2]*MyU.x + GPU_EleStiff[LID1][LID2+8]*MyU.y + GPU_EleStiff[LID1][LID2+16]*MyU.z);
													MyRes.y -= Dens*(GPU_EleStiff[LID1+8][LID2]*MyU.x + GPU_EleStiff[LID1+8][LID2+8]*MyU.y + GPU_EleStiff[LID1+8][LID2+16]*MyU.z);
													MyRes.z -= Dens*(GPU_EleStiff[LID1+16][LID2]*MyU.x + GPU_EleStiff[LID1+16][LID2+8]*MyU.y + GPU_EleStiff[LID1+16][LID2+16]*MyU.z);
												}
											}
										}//end e2 loops
									}//if i okay
								}//i-loop
							}//if j okay
						}//j-loop
					}//if k okay
				}//k-loop
			}//end if not dirichlet
			//store results
			res[indg_cur] = MyRes;
		}//end if active
		__syncthreads();
	}
}

dim3 Scalar4Grid;
dim3 Scalar4Block;

//to be launched by 2D grid
//Sum of a 1D vector as needed e.g. during the computation of the compliance objective, one value per element!
__global__ void Sum1Kernel(REAL *d_result, REAL *a, const int pitchX, const int NX, const int NY, const int NZ)
{
#define SUMBLOCKX SCALAR4BLOCKX
#define SUMBLOCKY SCALAR4BLOCKY
	__shared__ REAL Accum[SUMBLOCKX*SUMBLOCKY];
	REAL sum = 0.0;
    const int ind = INDEX(threadIdx.x, threadIdx.y, SUMBLOCKX);
    const int i = INDEX(threadIdx.x, blockIdx.x, SUMBLOCKX);
    const int j = INDEX(threadIdx.y, blockIdx.y, SUMBLOCKY);
    const int active = (i >= 0 && i<NX-1 && j >=0 && j<NY-1);
    for(int k=0;k<NZ-1;k++)
    {
		const int indg = INDEX3D(i, j, k, pitchX, NY);
		
		REAL MyA;
		if(active)
        {
			MyA = a[indg];
		}
        else
        {
			MyA = 0.0;
		}
		Accum[ind] = MyA;
		__syncthreads();
        //tree reduction
		for(int stride = SUMBLOCKX*SUMBLOCKY/2; stride>0; stride/=2)
        {
			if(ind < stride)
            {
				REAL Result = Accum[ind];
                Result += Accum[ind+stride];
                Accum[ind] = Result;
			}
            __syncthreads();
		}
		sum += Accum[0];
	}
	if(ind == 0)
    {
		d_result[blockIdx.x+blockIdx.y*gridDim.x] = sum;
	}
}

//to be launched by 2D grid
//Sum the volume of the state, one value per element!
__global__ void SumVolumeKernel(REAL *d_result, REAL4 *a, const int pitchX, const int NX, const int NY, const int NZ)
{
	__shared__ REAL Accum[SUMBLOCKX*SUMBLOCKY];
	REAL sum = 0.0;
    const int ind = INDEX(threadIdx.x, threadIdx.y, SUMBLOCKX);
    const int i = INDEX(threadIdx.x, blockIdx.x, SUMBLOCKX);
    const int j = INDEX(threadIdx.y, blockIdx.y, SUMBLOCKY);
    const int active = (i >= 0 && i<NX-1 && j >=0 && j<NY-1);
    for(int k=0;k<NZ-1;k++)
    {
		const int indg = INDEX3D(i, j, k, pitchX, NY);
		
		REAL MyA;
		if(active)
        {
			MyA = a[indg].w;
		}
        else
        {
			MyA = 0.0;
		}
		Accum[ind] = MyA;
		__syncthreads();
        //tree reduction
		for(int stride = SUMBLOCKX*SUMBLOCKY/2; stride>0; stride/=2)
        {
			if(ind < stride)
            {
				REAL Result = Accum[ind];
                Result += Accum[ind+stride];
                Accum[ind] = Result;
			}
            __syncthreads();
		}
		sum += Accum[0];
	}
	if(ind == 0)
    {
		d_result[blockIdx.x+blockIdx.y*gridDim.x] = sum;
	}
}

//Scalarproduct Kernel for REAL4 vectors as needed e.g. during CG iteration
__global__ void ScalarProd4Kernel(REAL *d_result, REAL4 *a, REAL4 *b, const int pitchX, const int NX, const int NY, const int NZ)
{
	__shared__ REAL4 Accum[SCALAR4BLOCKX*SCALAR4BLOCKY];
	REAL sum = 0.0;
	int i = threadIdx.x;
	int j = threadIdx.y;
	const int ind = INDEX(i,j,SCALAR4BLOCKX);
	i = INDEX(i, blockIdx.x, SCALAR4BLOCKX);
	j = INDEX(j, blockIdx.y, SCALAR4BLOCKY);
	const int active = (i >= 0 && i<NX && j >=0 && j<NY);
	int indg = INDEX(i,j,pitchX);
	for(int k=0;k<NZ;k++)
	{
		REAL4 MyA;
		REAL4 MyB;
		if(active)
		{
			MyA = a[indg];
			MyB = b[indg];
		}
		else
		{
			MyA.x = 0.0;
			MyA.y = 0.0;
			MyA.z = 0.0;
			MyB.x = 0.0;
			MyB.y = 0.0;
			MyB.z = 0.0;
		}
		REAL4 Result;
		Result.x = MyA.x*MyB.x;
		Result.y = MyA.y*MyB.y;
		Result.z = MyA.z*MyB.z;
		//skip density w
		Accum[ind] = Result;
		__syncthreads();
		//tree reduction
		for(int stride = SCALAR4BLOCKX*SCALAR4BLOCKY/2; stride>0;stride/=2)
		{
			if(ind < stride)
			{
				Result = Accum[ind];
				const REAL4 Result2 = Accum[ind+stride];
				Result.x += Result2.x;
				Result.y += Result2.y;
				Result.z += Result2.z;
				Accum[ind] = Result;
			}
			__syncthreads();
		}
		sum += Accum[0].x + Accum[0].y + Accum[0].z;
		indg = INDEX(indg, NY, pitchX);
	}
	if(ind == 0)
	{
		d_result[blockIdx.x+blockIdx.y*gridDim.x] = sum;
	}
}

#define p2 512

//to be launched by 1D Grid
__global__ void MyReduction(REAL *d_DataIn, REAL *d_DataOut)
{
	__shared__ REAL sdata[p2];
	
	// load shared mem
	unsigned int tid = threadIdx.x;
	unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
	sdata[tid] = d_DataIn[i];
	__syncthreads();
	
	// do reduction in shared mem
	for(unsigned int s=blockDim.x/2; s>0; s>>=1) 
	{
		if (tid < s) 
		{
			sdata[tid] += sdata[tid + s];
		}
		__syncthreads();
	}
	// write result for this block to global mem
	if (tid == 0) d_DataOut[blockIdx.x] = sdata[0];
}

//compute v[i] = a1*v[i] + a2*w[i]
__global__ void MyVecSMultAddKernel(REAL4 *v, const REAL a1, REAL4 *w, const REAL a2, const int pitchX, const int NX, const int NY, const int NZ)
{
	int i = threadIdx.x;
	int j = threadIdx.y;
	i = INDEX(i, blockIdx.x, BLOCKX);
	j = INDEX(j, blockIdx.y, BLOCKY);
	const int active = (i >= 0 && i<NX && j >=0 && j<NY);
	int indg = INDEX(i,j,pitchX);
	if(active)
	{
		for(int k=0;k<NZ;k++)
		{
			REAL4 MyV = v[indg];
			REAL4 MyW = w[indg];
			REAL4 result;
			result.x = a1*MyV.x + a2*MyW.x;
			result.y = a1*MyV.y + a2*MyW.y;
			result.z = a1*MyV.z + a2*MyW.z;
			//copy w
			result.w = MyV.w;
			v[indg] = result;
			indg = INDEX(indg, NY, pitchX);
		}
	}
}

__device__ inline int GPUGetLocalID(const int i, const int j, const int k)
{
	return i+2*j+4*k;
}

//new kernels for objective evaluation and gradient computation
//Strategy: One thread per ELEMENT!
__global__ void GPUEvalGrad(const int NX, const int NY, const int NZ, const int u_pitchX, const int grad_pitchX, REAL4 *d_u, REAL *d_grad, REAL *d_ObjValue, REAL gpupexp)
{
	//Strategy: One thread per ELEMENT
	//Every thread is a "compute" thread
	//MUST HAVE MORE INTERIOUR NODES THAN OUTSIDE NODES!!

	//The vector index this thread is responsible for:
	int indg;
	//The corresponding index in the shared memory:
	int ind;

	//some threads need to load halo-values additionally to being a compute thread
	//hence they require a second set of IDs for the halos they must load
	//in addition to being a "compute" thread
	int indg_h;
	//The corresponding "row" in the share memory:
	int ind_h;

	__shared__ REAL4 s_u[2*((BLOCKX+1)*(BLOCKY+1))];

	//local 1 ID index of the component being computed by this thread in this block
	//same as the 1D index of this thread
	int k = threadIdx.x + threadIdx.y*BLOCKX;
	//printf("(%d, %d): My k is %d. Halo Barrier: k < %d\n", threadIdx.x, threadIdx.y, k, (BLOCKX+BLOCKY+1));
	//some temp variables
	int i,j;
	
	//Does this thread have to additionally load halo values?
	bool halo = k < (BLOCKX+BLOCKY+1);
	//if this thread must load halo values set up indexes
	//non-divergent half-warp "if", if (BLOCKX+BLOCKY+1) divisible by 16
	//non-divergent warp "if", if divisible by 32
	
	if(halo)
	{
		//uncoalescenced y-halo: Nobrainer
		//non-divergent "if" as long as BLOCKX is a multiple of the half-warp size!!
		if(threadIdx.y==0)
		{
			i = threadIdx.x;
			j = BLOCKY;
		}
		//x-halo uncoalescenced
		else
		{
			i = BLOCKX;
			//produces sequence 0,1,2,3,..., BLOCKX+1
			j = k - BLOCKX;
		}
		//set the halo-index this thread needs to store to in the local array
		//store it in the 2nd layer of the 3D planes
		ind_h = INDEX(i,j,BLOCKX+1) + ((BLOCKX+1)*(BLOCKY+1));
		
		//set the halo-index this thread needs to load from the device array
		i = INDEX(i, blockIdx.x, BLOCKX);
		j = INDEX(j, blockIdx.y, BLOCKY);
		indg_h = INDEX(i, j, u_pitchX);
		
		//turn off halo-loading if the halo-point to load is outside
		//the computational domain
		halo = (i>=0) && (i<NX) && (j>=0) && (j<NY);
	}	
	//set-up the indices a thread always has to load:
	i = threadIdx.x;
	j = threadIdx.y;
	//index in the shared array to store to
	//store it in the 2nd layer of the 3D planes
	ind = INDEX(i,j,BLOCKX+1) + ((BLOCKX+1)*(BLOCKY+1));

	i = INDEX(i, blockIdx.x, BLOCKX);
	j = INDEX(j, blockIdx.y, BLOCKY);
	//index to load from in the device array
	indg = INDEX(i,j,u_pitchX);

	const int NXM1 = NX-1;
	const int NYM1 = NY-1;
	const int NZM1 = NZ-1;
	//shut-down if the GPU Grid is larger than the computational domain
	const bool ActiveMemory = (i<NX) && (j<NY);
	const bool ActiveCompute = (i<NXM1) && (j<NYM1);
	
	//regular load operation:
	if(ActiveMemory)
	{
		s_u[ind] = d_u[indg];
	}
	//halo load operation
	if(halo)
	{
	  	s_u[ind_h] = d_u[indg_h];
	}
	
	//loop in z-direction
	//int indg_cur; contains the index to store to in the original kernels
	REAL4 MyRes;
	
	//NZ is the number of points in Z-Direction. With one thread per element
	//one has NZ-1 elements in Z-Direction
	for(k=0;k<NZM1;k++)
	{
		//return i and j to thread index, not device memory index
		i = threadIdx.x;
		j = threadIdx.y;
		if(ActiveMemory)
		{
			//store current global index
			//indg_cur = indg;
			//move up one k-plane in the global IDs
			indg = INDEX(indg, NY, u_pitchX);
			//copy one plane backward in the shared array
			s_u[ind-((BLOCKX+1)*(BLOCKY+1))] = s_u[ind];
			if(k<NZM1)
			{
			  	s_u[ind] = d_u[indg];
			}
		}
		if(halo)
		{
			indg_h = INDEX(indg_h, NY, u_pitchX);
			s_u[ind_h-((BLOCKX+1)*(BLOCKY+1))] = s_u[ind_h];
			if(k<NZM1)
			{
				s_u[ind_h] = d_u[indg_h];
			}
		}
		
		//shared array finally contains all data!
		__syncthreads();
		
		//compute the actual matrix-vector product
		if(ActiveCompute)
		{
			//temp variables for storing K*u_e, e.g. the product of ref stiffness
			//with displacements.
			//ref stiffness is stored in __constant__ REAL GPU_EleStiff[24][24];
			REAL3 te0, te1, te2, te3, te4, te5, te6, te7;
			te0.x = 0.0;
			te0.y = 0.0;
			te0.z = 0.0;

			te1.x = 0.0;
			te1.y = 0.0;
			te1.z = 0.0;

			te2.x = 0.0;
			te2.y = 0.0;
			te2.z = 0.0;

			te3.x = 0.0;
			te3.y = 0.0;
			te3.z = 0.0;

			te4.x = 0.0;
			te4.y = 0.0;
			te4.z = 0.0;

			te5.x = 0.0;
			te5.y = 0.0;
			te5.z = 0.0;

			te6.x = 0.0;
			te6.y = 0.0;
			te6.z = 0.0;

			te7.x = 0.0;
			te7.y = 0.0;
			te7.z = 0.0;
			//for(int iMatVec=0;iMatVec<8;iMatVec++)
			for(int ek1=0;ek1<2;ek1++)
			{
				for(int ej1=0;ej1<2;ej1++)
				{
					for(int ei1=0;ei1<2;ei1++)
					{
						const int LocID = GPUGetLocalID(ei1, ej1, ek1);
						//k-offset is exactly (BLOCKX+1)*(BLOCKY+1)
						REAL4 MyU = s_u[INDEX(i+ei1, j+ej1, BLOCKX+1) + ek1*((BLOCKX+1)*(BLOCKY+1))];
						te0.x += GPU_EleStiff[0][LocID]*MyU.x;
						te0.x += GPU_EleStiff[0][LocID+8]*MyU.y;
						te0.x += GPU_EleStiff[0][LocID+16]*MyU.z;
						te1.x += GPU_EleStiff[1][LocID]*MyU.x;
						te1.x += GPU_EleStiff[1][LocID+8]*MyU.y;
						te1.x += GPU_EleStiff[1][LocID+16]*MyU.z;
						te2.x += GPU_EleStiff[2][LocID]*MyU.x;
						te2.x += GPU_EleStiff[2][LocID+8]*MyU.y;
						te2.x += GPU_EleStiff[2][LocID+16]*MyU.z;
						te3.x += GPU_EleStiff[3][LocID]*MyU.x;
						te3.x += GPU_EleStiff[3][LocID+8]*MyU.y;
						te3.x += GPU_EleStiff[3][LocID+16]*MyU.z;
						te4.x += GPU_EleStiff[4][LocID]*MyU.x;
						te4.x += GPU_EleStiff[4][LocID+8]*MyU.y;
						te4.x += GPU_EleStiff[4][LocID+16]*MyU.z;
						te5.x += GPU_EleStiff[5][LocID]*MyU.x;
						te5.x += GPU_EleStiff[5][LocID+8]*MyU.y;
						te5.x += GPU_EleStiff[5][LocID+16]*MyU.z;
						te6.x += GPU_EleStiff[6][LocID]*MyU.x;
						te6.x += GPU_EleStiff[6][LocID+8]*MyU.y;
						te6.x += GPU_EleStiff[6][LocID+16]*MyU.z;
						te7.x += GPU_EleStiff[7][LocID]*MyU.x;
						te7.x += GPU_EleStiff[7][LocID+8]*MyU.y;
						te7.x += GPU_EleStiff[7][LocID+16]*MyU.z;

						te0.y += GPU_EleStiff[8][LocID]*MyU.x;
						te0.y += GPU_EleStiff[8][LocID+8]*MyU.y;
						te0.y += GPU_EleStiff[8][LocID+16]*MyU.z;
						te1.y += GPU_EleStiff[9][LocID]*MyU.x;
						te1.y += GPU_EleStiff[9][LocID+8]*MyU.y;
						te1.y += GPU_EleStiff[9][LocID+16]*MyU.z;
						te2.y += GPU_EleStiff[10][LocID]*MyU.x;
						te2.y += GPU_EleStiff[10][LocID+8]*MyU.y;
						te2.y += GPU_EleStiff[10][LocID+16]*MyU.z;
						te3.y += GPU_EleStiff[11][LocID]*MyU.x;
						te3.y += GPU_EleStiff[11][LocID+8]*MyU.y;
						te3.y += GPU_EleStiff[11][LocID+16]*MyU.z;
						te4.y += GPU_EleStiff[12][LocID]*MyU.x;
						te4.y += GPU_EleStiff[12][LocID+8]*MyU.y;
						te4.y += GPU_EleStiff[12][LocID+16]*MyU.z;
						te5.y += GPU_EleStiff[13][LocID]*MyU.x;
						te5.y += GPU_EleStiff[13][LocID+8]*MyU.y;
						te5.y += GPU_EleStiff[13][LocID+16]*MyU.z;
						te6.y += GPU_EleStiff[14][LocID]*MyU.x;
						te6.y += GPU_EleStiff[14][LocID+8]*MyU.y;
						te6.y += GPU_EleStiff[14][LocID+16]*MyU.z;
						te7.y += GPU_EleStiff[15][LocID]*MyU.x;
						te7.y += GPU_EleStiff[15][LocID+8]*MyU.y;
						te7.y += GPU_EleStiff[15][LocID+16]*MyU.z;

						te0.z += GPU_EleStiff[16][LocID]*MyU.x;
						te0.z += GPU_EleStiff[16][LocID+8]*MyU.y;
						te0.z += GPU_EleStiff[16][LocID+16]*MyU.z;
						te1.z += GPU_EleStiff[17][LocID]*MyU.x;
						te1.z += GPU_EleStiff[17][LocID+8]*MyU.y;
						te1.z += GPU_EleStiff[17][LocID+16]*MyU.z;
						te2.z += GPU_EleStiff[18][LocID]*MyU.x;
						te2.z += GPU_EleStiff[18][LocID+8]*MyU.y;
						te2.z += GPU_EleStiff[18][LocID+16]*MyU.z;
						te3.z += GPU_EleStiff[19][LocID]*MyU.x;
						te3.z += GPU_EleStiff[19][LocID+8]*MyU.y;
						te3.z += GPU_EleStiff[19][LocID+16]*MyU.z;
						te4.z += GPU_EleStiff[20][LocID]*MyU.x;
						te4.z += GPU_EleStiff[20][LocID+8]*MyU.y;
						te4.z += GPU_EleStiff[20][LocID+16]*MyU.z;
						te5.z += GPU_EleStiff[21][LocID]*MyU.x;
						te5.z += GPU_EleStiff[21][LocID+8]*MyU.y;
						te5.z += GPU_EleStiff[21][LocID+16]*MyU.z;
						te6.z += GPU_EleStiff[22][LocID]*MyU.x;
						te6.z += GPU_EleStiff[22][LocID+8]*MyU.y;
						te6.z += GPU_EleStiff[22][LocID+16]*MyU.z;
						te7.z += GPU_EleStiff[23][LocID]*MyU.x;
						te7.z += GPU_EleStiff[23][LocID+8]*MyU.y;
						te7.z += GPU_EleStiff[23][LocID+16]*MyU.z;
					} //end ei1-loop
				} //end ej1-loop
			} //end ek1-loop
			//compute u_e^T * te = u_e^T*K*u_e
			REAL Grad_e = 0.0;
			//1D loc index 0 corresponds to 3d index vertex (0,0,0)
			REAL4 MyU = s_u[INDEX(i+0, j+0, BLOCKX+1) + 0*((BLOCKX+1)*(BLOCKY+1))];
			//density for this element is stored in ue0
			//compute density derivative
			const REAL dDens = pexp*pow(MyU.w, pexp-(REAL)1.0);
			const REAL Dens = pow(MyU.w, pexp);

			Grad_e += MyU.x*te0.x + MyU.y*te0.y + MyU.z*te0.z;
			//1D loc index 1 corresponds to 3d index vertex (1,0,0)
			MyU = s_u[INDEX(i+1, j+0, BLOCKX+1) + 0*((BLOCKX+1)*(BLOCKY+1))];
			Grad_e += MyU.x*te1.x + MyU.y*te1.y + MyU.z*te1.z;
			//1D loc index 2 corresponds to 3d index vertex (0,1,0)
			MyU = s_u[INDEX(i+0, j+1, BLOCKX+1) + 0*((BLOCKX+1)*(BLOCKY+1))];
			Grad_e += MyU.x*te2.x + MyU.y*te2.y + MyU.z*te2.z;
			//1D loc index 3 corresponds to 3d index vertex (1,1,0)
			MyU = s_u[INDEX(i+1, j+1, BLOCKX+1) + 0*((BLOCKX+1)*(BLOCKY+1))];
			Grad_e += MyU.x*te3.x + MyU.y*te3.y + MyU.z*te3.z;
			//1D loc index 4 corresponds to 3d index vertex (0,0,1)
			MyU = s_u[INDEX(i+0, j+0, BLOCKX+1) + 1*((BLOCKX+1)*(BLOCKY+1))];
			Grad_e += MyU.x*te4.x + MyU.y*te4.y + MyU.z*te4.z;
			//1D loc index 5 corresponds to 3d index vertex (1,0,1)
			MyU = s_u[INDEX(i+1, j+0, BLOCKX+1) + 1*((BLOCKX+1)*(BLOCKY+1))];
			Grad_e += MyU.x*te5.x + MyU.y*te5.y + MyU.z*te5.z;
			//1D loc index 6 corresponds to 3d index vertex (0,1,1)
			MyU = s_u[INDEX(i+0, j+1, BLOCKX+1) + 1*((BLOCKX+1)*(BLOCKY+1))];
			Grad_e += MyU.x*te6.x + MyU.y*te6.y + MyU.z*te6.z;
			//1D loc index 7 corresponds to 3d index vertex (1,1,1)
			MyU = s_u[INDEX(i+1, j+1, BLOCKX+1) + 1*((BLOCKX+1)*(BLOCKY+1))];
			Grad_e += MyU.x*te7.x + MyU.y*te7.y + MyU.z*te7.z;
			const REAL ObjValue = Dens*Grad_e;
			Grad_e *= -1.0*dDens;

			i = INDEX(i, blockIdx.x, BLOCKX);
			j = INDEX(j, blockIdx.y, BLOCKY);

			const int MyStoreID = INDEX3D(i,j,k, grad_pitchX, NY);
			d_grad[MyStoreID] = Grad_e;
			d_ObjValue[MyStoreID] = ObjValue;
		}//end if active
		__syncthreads();
	}
}
